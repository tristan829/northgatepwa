import { StatusCodeError } from '../errors/StatusCodeError';

export const BASE_URL = "https://react-nd-driver.web.app/admin/driver/login"//process.env.REACT_APP_BASE_URL;

export default function fetchData(route, option = {}) {
    return fetch(`${BASE_URL}/${route}`, option)
        .then(handleStatusCodeError);
}

export function handleStatusCodeError(response) {
    if (!response.ok) {
        throw new StatusCodeError(response);
    }

    return response;
}
