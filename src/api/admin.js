import fetchData from './fetchData';
// https://react-nd-driver.web.app/admin/driver/login
const driver = {
    login: (accountNumber, password) => fetchData('admin/driver/login', {
        method: 'POST',
        body: new URLSearchParams({
            accountNumber,
            password,
        }),
    })
        .then((response) => response.json()),
    getAll: () => fetchData('admin/driver/getAll', {
        method: 'GET',
    })
        .then((response) => response.json())
        .then((data) => data.driverList),
};

const freightBill = {
    getWaitingForDriverToPickUpList: (driverId) => fetchData(`admin/freightBill/getWaitingForDriverToPickUpList?driverId=${driverId}`, {
        method: 'GET',
    })
        .then((response) => response.json())
        .then((data) => data.freightBillGroupByAddressList),
    markAsDriverPicked: (targetItemSerialNumberArray, driverId) => (
        fetchData('admin/freightBill/markAsDriverPicked', {
            method: 'POST',
            body: JSON.stringify({
                targetItemSerialNumberArray,
                driverId,
            }),
        })),
    markAsDelivered: (targetItemSerialNumberArray, imageFileIdArray, isCodCollected, isDutyCollected) => (
        fetchData('admin/freightBill/markAsDelivered', {
            method: 'POST',
            body: JSON.stringify({
                targetItemSerialNumberArray,
                imageFileIdArray,
                isCodCollected,
                isDutyCollected,
            }),
        })),
    getInTransitList: (driverId) => fetchData(`admin/freightBill/getInTransitList?driverId=${driverId}`, {
        method: 'GET',
    })
        .then((response) => response.json())
        .then((data) => data.freightBillGroupByAddressList),
    reportException: (targetItemSerialNumberArray, exceptionReasonId) => (
        fetchData('admin/freightBill/reportException', {
            method: 'POST',
            body: JSON.stringify({
                targetItemSerialNumberArray,
                exceptionReasonId,
            }),
        })),
};

const exceptionReason = {
    getAll: () => fetchData('admin/exceptionReason/getAll', {
        method: 'GET',
    })
        .then((response) => response.json())
        .then((data) => data.exceptionReasonList),
};

export default {
    driver,
    freightBill,
    exceptionReason,
};
