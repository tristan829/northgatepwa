import fetchData, {
    BASE_URL,
} from './fetchData';

function uploadFile(file) {
    const form = new FormData();
    form.append('file', file);
    return fetchData('files', {
        method: 'POST',
        body: form,
    })
        .then((res) => res.json())
        .then((data) => data.fileId);
}

function getFile(fileId) {
    return fetchData('files', {
        method: 'GET',
        data: {
            fileId,
        },
    });
}

function getFileUrl(fileId) {
    return `${BASE_URL}/files?fileId=${fileId}`;
}

export default {
    uploadFile,
    getFile,
    getFileUrl,
};
