import admin from './admin';
import file from './files';

export default {
    admin,
    file,
};
