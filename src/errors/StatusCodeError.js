export class StatusCodeError extends Error {
    constructor(response) {
        super();
        this.name = 'StatusCodeError';
        this.message = 'status code not 200';
        this.response = response;
    }
}
