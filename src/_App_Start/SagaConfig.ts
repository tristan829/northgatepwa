import { all, spawn } from 'redux-saga/effects';
import { vendorsPageRootSaga } from '@views/vendors-page';
import { vendorsEditPageRootSaga } from '@views/vendors-edit-page';
import { driversPageRootSaga } from '@views/drivers-page';
import { driversCreationPageRootSaga } from '@views/drivers-creation-page';
import { driversEditPageRootSaga } from '@views/drivers-edit-page';

import { deliveryAreaBindingsPageRootSaga } from '@views/delivery-area-bindings-page';
import { deliveryAreaBindingsCreationPageRootSaga } from '@views/delivery-area-bindings-creation-page';
import { deliveryAreaBindingsEditPageRootSaga } from '@views/delivery-area-bindings-edit-page';

import { deliveryAreasPageRootSaga } from '@views/delivery-areas-page';
import { deliveryAreasCreationPageRootSaga } from '@views/delivery-areas-creation-page';
import { deliveryAreasEditPageRootSaga } from '@views/delivery-areas-edit-page';
import { exceptionReasonsPageRootSaga } from '@views/exception-reasons-page';
import { exceptionReasonsCreationPageRootSaga } from '@views/exception-reasons-creation-page';
import { exceptionReasonsEditPageRootSaga } from '@views/exception-reasons-edit-page';
import { adminUsersPageRootSaga } from '@views/admin-users-page';
import { adminUsersCreationPageRootSaga } from '@views/admin-users-creation-page';
import { adminUsersEditPageRootSaga } from '@views/admin-users-edit-page';
import { vendorBillReportsPageRootSaga } from '@views/vendor-bill-reports-page';
import { distributionCenterReceptionsPageRootSaga } from '@views/distribution-center-receptions-page';
import { batchUpdateDeliveredsPageRootSaga } from '@views/batch-update-delivereds-page';
import { northgateStationReceptionPageRootSaga } from '@views/northgate-station-receptions-page';
import { batchImportTaxsPageRootSaga } from '@views/batch-import-taxs-page';
import { batchImportTransferNumbersPageRootSaga } from '@views/batch-import-transfer-numbers-page';
import { deliveryFreightBillReportsPageRootSaga } from '@views/delivery-freight-bill-reports-page';
import { tallyImportPageRootSaga } from '@views/tally-import-page';
import { tallyFreightsPageRootSaga } from '@views/tally-freights-page';
import { tallyFreightsCreationPageRootSaga } from '@views/tally-freights-creation-page';
import { tallyFreightsEditPageRootSaga } from '@views/tally-freights-edit-page';
import { vendorPaymentInfosPageRootSaga } from '@views/vendor-payment-infos-page';
import { vendorPaymentInfosEditPageRootSaga } from '@views/vendor-payment-infos-edit-page';
import { tallySheetsPageRootSaga } from '@views/tally-sheets-page';
import { tallySheetsEditPageRootSaga } from '@views/tally-sheets-edit-page';

const pageSagas = [
  spawn(vendorsPageRootSaga),
  spawn(vendorsEditPageRootSaga),
  spawn(driversPageRootSaga),
  spawn(driversCreationPageRootSaga),
  spawn(driversEditPageRootSaga),

  spawn(deliveryAreaBindingsPageRootSaga),
  spawn(deliveryAreaBindingsCreationPageRootSaga),
  spawn(deliveryAreaBindingsEditPageRootSaga),
  spawn(deliveryAreasPageRootSaga),
  spawn(deliveryAreasCreationPageRootSaga),
  spawn(deliveryAreasEditPageRootSaga),
  spawn(exceptionReasonsPageRootSaga),
  spawn(exceptionReasonsCreationPageRootSaga),
  spawn(exceptionReasonsEditPageRootSaga),
  spawn(adminUsersPageRootSaga),
  spawn(adminUsersCreationPageRootSaga),
  spawn(adminUsersEditPageRootSaga),
  spawn(vendorBillReportsPageRootSaga),
  spawn(distributionCenterReceptionsPageRootSaga),
  spawn(batchUpdateDeliveredsPageRootSaga),
  spawn(northgateStationReceptionPageRootSaga),
  spawn(batchImportTaxsPageRootSaga),
  spawn(batchImportTransferNumbersPageRootSaga),
  spawn(deliveryFreightBillReportsPageRootSaga),
  spawn(tallyImportPageRootSaga),
  spawn(tallyFreightsPageRootSaga),
  spawn(tallyFreightsCreationPageRootSaga),
  spawn(tallyFreightsEditPageRootSaga),
  spawn(tallySheetsPageRootSaga),
  spawn(tallySheetsEditPageRootSaga),
  spawn(vendorPaymentInfosPageRootSaga),
  spawn(vendorPaymentInfosEditPageRootSaga),
];

const moduleSagas = [];

export function* rootSaga() {
  yield all([
    // ...moduleSagas,
    ...pageSagas,
  ]);
}
