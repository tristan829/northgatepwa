import React from 'react';
import { Outlet, useRoutes } from 'react-router-dom';
import * as RouteConfig from './RouteConfig';
import { AdminLayout, MenuConfig } from '@views/admin-layout';
import { HomePage } from '@views/home';
import { LoginPage } from '@views/login';
import { VendorsPage } from '@views/vendors-page';
import { VendorsCreationPage } from '@views/vendors-creation-page';
import { VendorsEditPage } from '@views/vendors-edit-page';
import { DriversPage } from '@views/drivers-page';
import { DriversCreationPage } from '@views/drivers-creation-page';
import { DriversEditPage } from '@views/drivers-edit-page';

import { DeliveryAreaBindingsPage } from '@views/delivery-area-bindings-page';
import { DeliveryAreaBindingsCreationPage } from '@views/delivery-area-bindings-creation-page';
import { DeliveryAreasPage } from '@views/delivery-areas-page';
import { DeliveryAreasCreationPage } from '@views/delivery-areas-creation-page';
import { DeliveryAreasEditPage } from '@views/delivery-areas-edit-page';
import { ExceptionReasonsPage } from '@views/exception-reasons-page';
import { ExceptionReasonsCreationPage } from '@views/exception-reasons-creation-page';
import { ExceptionReasonsEditPage } from '@views/exception-reasons-edit-page';
import { AdminUsersPage } from '@views/admin-users-page';
import { AdminUsersCreationPage } from '@views/admin-users-creation-page';
import { AdminUsersEditPage } from '@views/admin-users-edit-page';
import { VendorBillReportsPage } from '@views/vendor-bill-reports-page';
import { DeliveryAreaBindingsEditPage } from '@views/delivery-area-bindings-edit-page';
import { DistributionCenterReceptionsPage } from '@views/distribution-center-receptions-page';
import { BatchUpdateDeliveredsPage } from '@views/batch-update-delivereds-page';
import { NorthgateStationReceptionsPage } from '@views/northgate-station-receptions-page';
import { BatchImportTaxsPage } from '@views/batch-import-taxs-page';
import { BatchImportTransferNumbersPage } from '@views/batch-import-transfer-numbers-page';
import { DeliveryFreightBillReportsPage } from '@views/delivery-freight-bill-reports-page';
import { TallyImportPage } from '@views/tally-import-page';
import { TallyFreightsPage } from '@views/tally-freights-page';
import { TallyFreightsCreationPage } from '@views/tally-freights-creation-page';
import { TallyFreightsEditPage } from '@views/tally-freights-edit-page';
import { VendorPaymentInfosPage } from '@views/vendor-payment-infos-page';
import { VendorPaymentInfosEditPage } from '@views/vendor-payment-infos-edit-page';
import { TallySheetsPage } from '@views/tally-sheets-page';
import { TallySheetsEditPage } from '@views/tally-sheets-edit-page';

/** Sidebar 頁籤設定 */
const MenuConfigs: MenuConfig[] = [
  {
    id: 'M01_F01',
    path: '',
    title: '基本資料作業',
    subMenu: [
      { id: 'M01_F01_P01', path: RouteConfig.VENDORS, title: '廠商資料' },
      { id: 'M01_F01_P02', path: RouteConfig.DRIVERS, title: '司機資料' },
      { id: 'M01_F01_P03', path: RouteConfig.TALLY_FREIGHTS, title: '理貨明細' },
      { id: 'M01_F01_P04', path: RouteConfig.TALLY_SHEETS, title: '派貨明細' },
      { id: 'M01_F01_P06', path: RouteConfig.EXCEPTIONREASONS, title: '例外原因管理' },
      { id: 'M01_F01_P07', path: RouteConfig.VENDOR_PAYMENT_INFOS, title: '廠商帳款設定' },
      { id: 'M01_F01_P08', path: RouteConfig.DELIVERY_AREAS, title: '派送區域代碼' },
      { id: 'M01_P09_F01', path: RouteConfig.DELIVERY_AREA_BINDINGS, title: '派送區域綁定' },
    ],
  },
  {
    id: 'M01_F02',
    path: '',
    title: '理貨驗貨作業',
    subMenu: [{ id: 'M01_F02_P01', path: RouteConfig.DISTRIBUTION_CENTER_RECEPTIONS, title: '理貨驗貨作業' }],
  },
  {
    id: 'M01_F03',
    path: '',
    title: '點貨作業',
    subMenu: [{ id: 'M01_F03_P01', path: RouteConfig.NORTHGATE_STATION_RECEPTIONS, title: '一般點貨作業' }],
  },
  {
    id: 'M01_F04',
    path: '',
    title: '資料匯入作業',
    subMenu: [
      { id: 'M01_F04_P01', path: RouteConfig.TALLY_IMPORT, title: '理貨明細匯入' },
      { id: 'M01_F04_P03', path: RouteConfig.BATCH_UPDATE_DELIVEREDS, title: '批次更新送達貨件' },
      { id: 'M01_F04_P04', path: RouteConfig.BATCH_IMPORT_TRANSFER_NUMBER, title: '批次匯入轉運單號' },
      { id: 'M01_F04_P05', path: RouteConfig.BATCH_IMPORT_TAX, title: '批次匯入稅金' },
    ],
  },
  {
    id: 'M01_F05',
    path: '',
    title: '帳款作業',
    subMenu: [{ id: 'M01_F05_P02', path: RouteConfig.VENDOR_BILL_REPORTS, title: '廠商帳款報表' }],
  },
  {
    id: 'M01_F07',
    path: RouteConfig.DELIVERY_FREIGHT_BILL_REPORTS,
    title: '派貨明細報表',
  },
  {
    id: 'M01_F08',
    path: RouteConfig.ADMINUSERS,
    title: '後臺帳號管理',
  },
];

/**
 * AdminLayout 元件
 */
const AdminLayoutOutlet: React.FC = () => (
  <AdminLayout menuConfigs={MenuConfigs}>
    <Outlet />
  </AdminLayout>
);

/**
 * 404 頁面元件
 *
 * TODO: 完善頁面內容
 */
const NotFound: React.FC = () => <div>404</div>;

/**
 * App Router 元件
 *
 * TODO: 實現 Auth Route
 */
export const AppRouterComponent = () => {
  const element = useRoutes([
    {
      path: RouteConfig.LOGIN,
      element: <LoginPage />,
    },
    {
      path: RouteConfig.ADMIN_BASE,
      element: <AdminLayoutOutlet />,
      children: [
        {
          path: RouteConfig.ADMIN_BASE,
          element: <HomePage />,
        },
        {
          path: RouteConfig.VENDORS,
          element: <VendorsPage />,
        },
        {
          path: RouteConfig.VENDORS_CREATE,
          element: <VendorsCreationPage />,
        },
        {
          path: `${RouteConfig.VENDORS_EDIT}/:id`,
          element: <VendorsEditPage />,
        },

        /** 司機資料 - 列表頁面 */
        {
          path: RouteConfig.DRIVERS,
          element: <DriversPage />,
        },

        /** 司機資料 - 新增頁面 */
        {
          path: RouteConfig.DRIVERS_CREATE,
          element: <DriversCreationPage />,
        },

        /** 司機資料 - 編輯頁面 */
        {
          path: `${RouteConfig.DRIVERS_EDIT}/:id`,
          element: <DriversEditPage />,
        },

        /** 理貨明細 - 列表頁面 */
        {
          path: `${RouteConfig.TALLY_FREIGHTS}`,
          element: <TallyFreightsPage />,
        },

        /** 理貨明細 - 新增頁面 */
        {
          path: `${RouteConfig.TALLY_FREIGHTS_CREATE}`,
          element: <TallyFreightsCreationPage />,
        },

        /** 理貨明細 - 編輯頁面 */
        {
          path: `${RouteConfig.TALLY_FREIGHTS_EDIT}/:id`,
          element: <TallyFreightsEditPage />,
        },

        /** 派貨明細(理貨單) - 列表頁面 */
        {
          path: `${RouteConfig.TALLY_SHEETS}`,
          element: <TallySheetsPage />,
        },

        /** 派貨明細(理貨單) - 編輯頁面 */
        {
          path: `${RouteConfig.TALLY_SHEETS_EDIT}/:id`,
          element: <TallySheetsEditPage />,
        },

        /** 派送區域綁定設定 - 列表頁面 */
        {
          path: `${RouteConfig.DELIVERY_AREA_BINDINGS}`,
          element: <DeliveryAreaBindingsPage />,
        },
        /** 派送區域綁定 - 新增頁面 */
        {
          path: `${RouteConfig.DELIVERY_AREA_BINDINGS_CREATE}`,
          element: <DeliveryAreaBindingsCreationPage />,
        },
        /** 派送區域綁定 - 編輯頁面 */
        {
          path: `${RouteConfig.DELIVERY_AREA_BINDINGS_EDIT}/:id`,
          element: <DeliveryAreaBindingsEditPage />,
        },
        /** 派貨區域代碼管理 - 列表頁面 */
        {
          path: `${RouteConfig.DELIVERY_AREAS}`,
          element: <DeliveryAreasPage />,
        },

        /** 派貨區域代碼管理 - 新增頁面 */
        {
          path: `${RouteConfig.DELIVERY_AREAS_CREATE}`,
          element: <DeliveryAreasCreationPage />,
        },

        /** 派貨區域代碼管理 - 編輯頁面 */
        {
          path: `${RouteConfig.DELIVERY_AREAS_EDIT}/:id`,
          element: <DeliveryAreasEditPage />,
        },

        /** 例外原因 - 列表頁面 */
        {
          path: `${RouteConfig.EXCEPTIONREASONS}`,
          element: <ExceptionReasonsPage />,
        },

        /** 例外原因 - 新增頁面 */
        {
          path: `${RouteConfig.EXCEPTIONREASONS_CREATE}`,
          element: <ExceptionReasonsCreationPage />,
        },

        /** 例外原因 - 編輯頁面 */
        {
          path: `${RouteConfig.EXCEPTIONREASONS_EDIT}/:id`,
          element: <ExceptionReasonsEditPage />,
        },

        /** 後臺管理 - 列表頁面 */
        {
          path: `${RouteConfig.ADMINUSERS}`,
          element: <AdminUsersPage />,
        },

        /** 後臺管理 - 新增頁面 */
        {
          path: `${RouteConfig.ADMINUSERS_CREATE}`,
          element: <AdminUsersCreationPage />,
        },

        /** 後臺管理 - 編輯頁面 */
        {
          path: `${RouteConfig.ADMINUSERS_EDIT}/:id`,
          element: <AdminUsersEditPage />,
        },
        /** 廠商帳款報表 - 列表頁面 */
        {
          path: `${RouteConfig.VENDOR_BILL_REPORTS}`,
          element: <VendorBillReportsPage />,
        },
        /** 理貨驗貨作業 - 列表頁面 */
        {
          path: `${RouteConfig.DISTRIBUTION_CENTER_RECEPTIONS}`,
          element: <DistributionCenterReceptionsPage />,
        },
        /** 一般點貨作業 - 單筆查詢頁面 */
        {
          path: `${RouteConfig.NORTHGATE_STATION_RECEPTIONS}`,
          element: <NorthgateStationReceptionsPage />,
        },

        /** 批次更新送達貨件 - 列表頁面 */
        {
          path: `${RouteConfig.BATCH_UPDATE_DELIVEREDS}`,
          element: <BatchUpdateDeliveredsPage />,
        },
        /** 批次匯入轉運單號 - 列表頁面 */
        {
          path: `${RouteConfig.BATCH_IMPORT_TRANSFER_NUMBER}`,
          element: <BatchImportTransferNumbersPage />,
        },
        /** 批次匯入稅金 - 列表頁面 */
        {
          path: `${RouteConfig.BATCH_IMPORT_TAX}`,
          element: <BatchImportTaxsPage />,
        },
        /** 派貨明細報表 - 列表頁面 */
        {
          path: `${RouteConfig.DELIVERY_FREIGHT_BILL_REPORTS}`,
          element: <DeliveryFreightBillReportsPage />,
        },

        /** 理貨明細匯入 列表頁面 */
        {
          path: `${RouteConfig.TALLY_IMPORT}`,
          element: <TallyImportPage />,
        },

        /** 廠商帳款設定 列表頁面 */
        {
          path: `${RouteConfig.VENDOR_PAYMENT_INFOS}`,
          element: <VendorPaymentInfosPage />,
        },
        /** 廠商帳款設定 編輯頁面 */
        {
          path: `${RouteConfig.VENDOR_PAYMENT_INFOS_EDIT}/:id`,
          element: <VendorPaymentInfosEditPage />,
        },
      ],
    },
    {
      path: '*',
      element: <NotFound />,
    },
  ]);

  return element;
};
