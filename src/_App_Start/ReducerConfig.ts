import { combineReducers } from 'redux';
import { routerReducer } from '@modules/router';
import { spinnerReducer } from '@modules/spinner';
import { homeModuleReducer } from '@views/home';
import { loginModuleReducer } from '@views/login';
import { vendorsPageReducer } from '@views/vendors-page';
import { vendorsEditPageReducer } from '@views/vendors-edit-page';
import { driversPageReducer } from '@views/drivers-page';
import { driversCreationPageReducer } from '@views/drivers-creation-page';
import { driversEditPageReducer } from '@views/drivers-edit-page';

import { deliveryAreaBindingsPageReducer } from '@views/delivery-area-bindings-page';
import { deliveryAreaBindingsCreationPageReducer } from '@views/delivery-area-bindings-creation-page';
import { deliveryAreaBindingsEditPageReducer } from '@views/delivery-area-bindings-edit-page';

import { deliveryAreasPageReducer } from '@views/delivery-areas-page';
import { deliveryAreasCreationPageReducer } from '@views/delivery-areas-creation-page';
import { deliveryAreasEditPageReducer } from '@views/delivery-areas-edit-page';
import { exceptionReasonsPageReducer } from '@views/exception-reasons-page';
import { exceptionReasonsCreationPageReducer } from '@views/exception-reasons-creation-page';
import { exceptionReasonsEditPageReducer } from '@views/exception-reasons-edit-page';
import { vendorBillReportsPageReducer } from '@views/vendor-bill-reports-page';
import { distributionCenterReceptionsPageReducer } from '@views/distribution-center-receptions-page';
import { batchUpdateDeliveredsPageReducer } from '@views/batch-update-delivereds-page';
import { northgateStationReceptionPageReducer } from '@views/northgate-station-receptions-page';
import { batchImportTaxsPageReducer } from '@views/batch-import-taxs-page';
import { batchImportTransferNumbersPageReducer } from '@views/batch-import-transfer-numbers-page';
import { adminUsersPageReducer } from '@views/admin-users-page';
import { adminUsersCreationPageReducer } from '@views/admin-users-creation-page';
import { adminUsersEditPageReducer } from '@views/admin-users-edit-page';
import { deliveryFreightBillReportsPageReducer } from '@views/delivery-freight-bill-reports-page';
import { tallyImportPageReducer } from '@views/tally-import-page';
import { tallyFreightsPageReducer } from '@views/tally-freights-page';
import { tallyFreightsCreationPageReducer } from '@views/tally-freights-creation-page';
import { tallyFreightsEditPageReducer } from '@views/tally-freights-edit-page';
import { vendorPaymentInfosPageReducer } from '@views/vendor-payment-infos-page';
import { vendorPaymentInfosEditPageReducer } from '@views/vendor-payment-infos-edit-page';
import { tallySheetsPageReducer } from '@views/tally-sheets-page';
import { tallySheetsEditPageReducer } from '@views/tally-sheets-edit-page';
/** Pages Reducer */
const pagesReducer = combineReducers({
  home: homeModuleReducer,
  login: loginModuleReducer,
  vendors: vendorsPageReducer,
  vendorsEdit: vendorsEditPageReducer,
  drivers: driversPageReducer,
  driversCreation: driversCreationPageReducer,
  driversEdit: driversEditPageReducer,

  deliveryAreaBindings: deliveryAreaBindingsPageReducer,
  deliveryAreaBindingsCreation: deliveryAreaBindingsCreationPageReducer,
  deliveryAreaBindingsEdit: deliveryAreaBindingsEditPageReducer,
  deliveryAreas: deliveryAreasPageReducer,
  deliveryAreasCreation: deliveryAreasCreationPageReducer,
  deliveryAreasEdit: deliveryAreasEditPageReducer,
  exceptionReasons: exceptionReasonsPageReducer,
  exceptionReasonsCreation: exceptionReasonsCreationPageReducer,
  exceptionReasonsEdit: exceptionReasonsEditPageReducer,
  adminUsers: adminUsersPageReducer,
  adminUsersCreation: adminUsersCreationPageReducer,
  adminUsersEdit: adminUsersEditPageReducer,
  vendorBillReports: vendorBillReportsPageReducer,
  distributionCenterReceptions: distributionCenterReceptionsPageReducer,
  batchUpdateDelivereds: batchUpdateDeliveredsPageReducer,
  northgateStationReceptions: northgateStationReceptionPageReducer,
  batchImportTaxs: batchImportTaxsPageReducer,
  batchImportTransferNumbers: batchImportTransferNumbersPageReducer,
  deliveryFreightBillReports: deliveryFreightBillReportsPageReducer,
  tallyImport: tallyImportPageReducer,

  tallyFreights: tallyFreightsPageReducer,
  tallyFreightsCreation: tallyFreightsCreationPageReducer,
  tallyFreightsEdit: tallyFreightsEditPageReducer,

  tallySheets: tallySheetsPageReducer,
  tallySheetsEdit: tallySheetsEditPageReducer,

  vendorPaymentInfos: vendorPaymentInfosPageReducer,
  vendorPaymentInfosEdit: vendorPaymentInfosEditPageReducer,
});

/** Root Reducer */
export const rootReducer = combineReducers({
  pages: pagesReducer,
  router: routerReducer,
  spinner: spinnerReducer,
});

export type RootReducer = ReturnType<typeof rootReducer>;
