export const ADMIN_BASE = '/admin';

/** 首頁 */
export const HOME = ADMIN_BASE;

/** 登入頁面 */
export const LOGIN = '/admin/login';

/** 廠商資料-列表頁面 */
export const VENDORS = '/admin/vendors';

/** 廠商資料-建立頁面 */
export const VENDORS_CREATE = '/admin/vendors/create';

/** 廠商資料-編輯頁面 */
export const VENDORS_EDIT = '/admin/vendors/edit';

/** 司機資料-列表頁面 */
export const DRIVERS = '/admin/drivers';

/** 司機資料-建立頁面 */
export const DRIVERS_CREATE = '/admin/drivers/create';

/** 司機資料-編輯頁面 */
export const DRIVERS_EDIT = '/admin/drivers/edit';

/** 理貨明細-列表頁面 */
export const TALLY_FREIGHTS = '/admin/tally-freights';

/** 理貨明細-新增頁面 */
export const TALLY_FREIGHTS_CREATE = '/admin/tally-freights/create';

/** 理貨明細-編輯頁面 */
export const TALLY_FREIGHTS_EDIT = '/admin/tally-freights/edit';

/** 派貨明細(理貨單)-列表頁面 */
export const TALLY_SHEETS = '/admin/tally-sheets';

/** 派貨明細(理貨單)-新增頁面 */
export const TALLY_SHEETS_CREATE = '/admin/tally-sheets/create';

/** 派貨明細(理貨單)-編輯頁面 */
export const TALLY_SHEETS_EDIT = '/admin/tally-sheets/edit';

/** 派送區域綁定設定-列表頁面 */
export const DELIVERY_AREA_BINDINGS = '/admin/delivery-area-bindings';

/** 派送區域綁定設定-新增頁面 */
export const DELIVERY_AREA_BINDINGS_CREATE = '/admin/delivery-area-bindings/create';

/** 派送區域綁定設定-編輯頁面 */
export const DELIVERY_AREA_BINDINGS_EDIT = '/admin/delivery-area-bindings/edit';

/** 派貨區域代碼管理-列表頁面 */
export const DELIVERY_AREAS = '/admin/delivery-areas';

/** 派貨區域代碼管理-新增頁面 */
export const DELIVERY_AREAS_CREATE = '/admin/delivery-areas/create';

/** 派貨區域代碼管理-編輯頁面 */
export const DELIVERY_AREAS_EDIT = '/admin/delivery-areas/edit';

/** 例外原因-列表頁面 */
export const EXCEPTIONREASONS = '/admin/exception-reasons';

/** 例外原因-新增頁面 */
export const EXCEPTIONREASONS_CREATE = '/admin/exception-reasons/create';

/** 例外原因-編輯頁面 */
export const EXCEPTIONREASONS_EDIT = '/admin/exception-reasons/edit';

/** 帳號管理-列表頁面 */
export const ADMINUSERS = '/admin/admin-users';

/** 帳號管理-新增頁面 */
export const ADMINUSERS_CREATE = '/admin/admin-users/create';

/** 帳號管理-編輯頁面 */
export const ADMINUSERS_EDIT = '/admin/admin-users/edit';
/** 廠商帳款報表-列表頁面 */
export const VENDOR_BILL_REPORTS = '/admin/vendor-bill-reports';

/** 理貨驗貨作業-列表頁面 */
export const DISTRIBUTION_CENTER_RECEPTIONS = '/admin/distribution-center-receptions';

/** 批次更新送達貨件-列表頁面 */
export const BATCH_UPDATE_DELIVEREDS = '/admin/batch-update-delivereds';

/** 點貨作業-單筆查詢頁面 */
export const NORTHGATE_STATION_RECEPTIONS = '/admin/northgate-station-receptions';
/** 批次匯入稅金-列表頁面 */
export const BATCH_IMPORT_TAX = '/admin/batch-import-taxs';

/** 批次匯入轉運單號-列表頁面 */
export const BATCH_IMPORT_TRANSFER_NUMBER = '/admin/batch-import-transfer-numbers';

/** 派貨明細報表-列表頁面 */
export const DELIVERY_FREIGHT_BILL_REPORTS = '/admin/delivery-freight-bill-reports';
/** 理貨明細匯入 列表頁面 */
export const TALLY_IMPORT = '/admin/tally-import';

/** 廠商帳款設定 列表頁面 */
export const VENDOR_PAYMENT_INFOS = '/admin/vendor-payment-infos';
/** 廠商帳款設定 編輯頁面 */
export const VENDOR_PAYMENT_INFOS_EDIT = '/admin/vendor-payment-infos/edit';
