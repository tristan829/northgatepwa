import { useContext } from 'react';
import { AlertProvider, AlertContext } from './components/AlertProvider';

export const useAlert = () => useContext(AlertContext);
export default AlertProvider;
