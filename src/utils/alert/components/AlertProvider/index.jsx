import React, { useState } from 'react';

import { useTheme } from '@material-ui/core/styles';
import { createStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { faCheck, faExclamation, faTimes } from '@fortawesome/fontawesome-free';

export const AlertContext = React.createContext({
    show: () => {},
});
const useStyles = makeStyles((theme) => createStyles({
    root: {
        '& .MuiDialog-paperWidthSm': {
            minWidth: 400,
        },
    },
    title: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(2),
        paddingBottom: 0,
        fontSize: theme.typography.h6.fontSize,
        fontWeight: theme.typography.fontWeightBold,
    },
    actions: {
        justifyContent: 'flex-end',
        padding: theme.spacing(2),
    },
    content: {
        width: '100%',
        maxHeight: 240,
        padding: theme.spacing(2),
        overflowY: 'auto',
    },
    icon: {
        color: theme.palette.common.white,
        padding: theme.spacing(1),
        borderRadius: theme.shape.borderRadius,
        marginRight: theme.spacing(2),
    },
}));
export function AlertProvider(props) {
    const { children } = props;
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const [type, setType] = useState('default');
    const [title, setTitle] = useState('');
    const [canceledOnTouchOutside, setCanceledOnTouchOutside] = useState(false);
    const [onConfirm, setOnConfirm] = useState(() => () => { });
    const [message, setMessage] = useState('');
    const [showCancelButton, setShowCancelButton] = useState(false);
    function show(option) {
        setType(option.type || 'default');
        setTitle(option.title || '');
        setMessage(option.message || '');
        setCanceledOnTouchOutside(option.canceledOnTouchOutside || false);
        if (option.onConfirm) {
            setOnConfirm(() => option.onConfirm);
        }
        else {
            setOnConfirm(() => () => { });
        }
        setShowCancelButton(option.cancelButton || false);
        setOpen(true);
    }
    function handleOnClose() {
        if (canceledOnTouchOutside) {
            setOpen(false);
        }
    }
    function handleConfirmClick() {
        setOpen(false);
        onConfirm();
    }
    function renderTitleIcon() {
        if (type === 'default')
            return '';
        let color = '';
        let icon = 'question';
        switch (type) {
            case 'success':
                color = theme.palette.success.main;
                icon = faCheck;
                break;
            case 'waring':
                color = theme.palette.warning.main;
                icon = faExclamation;
                break;
            case 'error':
                color = theme.palette.error.main;
                icon = faTimes;
                break;
        }
        return (
            <div className={classes.icon} style={{ backgroundColor: color }}>
                <Icon fixedWidth icon={icon} />
            </div>
        );
    }
    return (
        <AlertContext.Provider value={{ show }}>
            <Dialog className={classes.root} open={open} onClose={handleOnClose}>
                {title ? (
                    <div className={classes.title}>
                        {renderTitleIcon()}
                        {title}
                    </div>
                ) : null}
                {message ? (
                    <div className={classes.content}>
                        <Typography color="textSecondary" style={{ whiteSpace: 'pre-wrap' }}>
                            {message}
                        </Typography>
                    </div>
                ) : null}
                <DialogActions className={classes.actions}>
                    {showCancelButton ? (
                        <Button variant="outlined" color="default" disableElevation onClick={() => setOpen(false)}>
                            取消
                        </Button>
                    ) : null}
                    <Button variant="contained" color="secondary" disableElevation onClick={handleConfirmClick}>
                        確定
                    </Button>
                </DialogActions>
            </Dialog>
            {children}
        </AlertContext.Provider>
    );
}
