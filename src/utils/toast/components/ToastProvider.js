import React, {
    useState,
} from 'react';
import * as PropTypes from 'prop-types';
import MuiAlert from '@material-ui/lab/Alert';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Snackbar from '@material-ui/core/Snackbar';

export const ToastContext = React.createContext({});

const ToastSeverity = {
    SUCCESS: 'success',
    INFO: 'info',
    ERROR: 'error',
};

ToastProvider.propTypes = {
    children: PropTypes.node.isRequired,
};

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
});

function ToastProvider(props) {
    const {
        children,
    } = props;
    const classes = useStyles();

    const [open, setOpen] = useState(false);
    const [autoHideDuration, setAutoHideDuration] = useState(600000);
    const [message, setMessage] = useState('');
    const [severity, setSeverity] = useState('info');
    function handleClose() {
        setOpen(false);
    }

    function showToast(message, autoHideDuration = 600000, severity) {
        setMessage(message);
        setAutoHideDuration(autoHideDuration);
        setSeverity(severity);
        setOpen(true);
    }

    function showSuccessToast(message, autoHideDuration) {
        showToast(message, autoHideDuration, ToastSeverity.SUCCESS);
    }

    function showInfoToast(message, autoHideDuration) {
        showToast(message, autoHideDuration, ToastSeverity.INFO);
    }

    function showErrorToast(message, autoHideDuration) {
        showToast(message, autoHideDuration, ToastSeverity.ERROR);
    }

    return (
        <ToastContext.Provider
            value={{
                showSuccessToast,
                showErrorToast,
                showInfoToast,
            }}
        >
            <Snackbar open={open} autoHideDuration={autoHideDuration} onClose={handleClose} style={{ bottom: 16 }}>
                <MuiAlert
                    className={classes.root}
                    elevation={1}
                    onClose={handleClose}
                    severity={severity}
                >
                    {message}
                </MuiAlert>
            </Snackbar>
            {children}
        </ToastContext.Provider>
    );
}

export default ToastProvider;
