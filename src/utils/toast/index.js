import {
    useContext,
} from 'react';

import ToastProvider, {
    ToastContext,
} from './components/ToastProvider.js';

/**
 *
 * @returns {{
 *     showSuccessToast,
 *     showInfoToast,
 *     showErrorToast,
 * }}
 */
export function useToast() {
    return useContext(ToastContext);
}

export default ToastProvider;
