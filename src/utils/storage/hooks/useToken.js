import { useStorage } from '../index';

export const useToken = () => {
    const { storage, updateStorage } = useStorage();

    function setToken(token) {
        updateStorage({
            token,
        });
    }

    function clearToken() {
        updateStorage({
            token: '',
        });
    }

    return {
        token: storage.token,
        setToken,
        clearToken,
    };
};
