import React, {
    useState,
    useEffect,
} from 'react';
import * as PropTypes from 'prop-types';

export const StorageContext = React.createContext({});

const initStorage = {};
const STORAGE_KEY = 'WENSHENG_STORAGE';

function StorageProvider(props) {
    const {
        children,
    } = props;

    const [storage, setStorage] = useState(initStorage);

    useEffect(() => {
        const localStorageData = localStorage.getItem(STORAGE_KEY);
        if (localStorageData) {
            try {
                setStorage(JSON.parse(localStorageData));
            } catch (e) {
                updateStorage({});
            }
        } else {
            updateStorage({});
        }
    }, []);

    function updateStorage(newStorage) {
        localStorage.setItem(STORAGE_KEY, JSON.stringify({
            ...storage,
            ...newStorage,
        }));

        setStorage({
            ...storage,
            ...newStorage,
        });
    }

    return (
        <StorageContext.Provider value={{ storage, updateStorage }}>
            {children}
        </StorageContext.Provider>
    );
}

StorageProvider.propTypes = {
    children: PropTypes.node.isRequired,
};

export default StorageProvider;
