import {
    useContext,
} from 'react';

import {
    StorageContext,
} from './components/StorageProvider';


/**
 *
 * @returns {{ storage, updateStorage }}
 */
export const useStorage = () => useContext(StorageContext);

export { default as StorageProvider } from './components/StorageProvider';
