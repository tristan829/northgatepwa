import {
    useContext,
} from 'react';

import LoadingProvider, {
    LoadingContext,
} from './components/LoadingProvider';

/**
 *
 * @returns {{ setIsLoading }}
 */
export const useLoading = () => useContext(LoadingContext);

export default LoadingProvider;
