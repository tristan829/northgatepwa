import React, {
    useState,
} from 'react';
import * as PropTypes from 'prop-types';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';




export const LoadingContext = React.createContext({});

function LoadingProvider(props) {
    const {
        children,
    } = props;

    const [isLoading, setIsLoading] = useState(false);

    return (
        <LoadingContext.Provider value={{ isLoading, setIsLoading }}>
            <Backdrop open={isLoading} style={{ zIndex: 1110 }}>
                <CircularProgress />
            </Backdrop>
            {children}
        </LoadingContext.Provider>
    );
}

LoadingProvider.propTypes = {
    children: PropTypes.node.isRequired,
};

export default LoadingProvider;
