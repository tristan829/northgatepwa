import React from 'react';
import {
    BrowserRouter,
    Route,
    Switch,
    Redirect,
} from 'react-router-dom';

import { useToken } from './utils/storage/hooks/useToken';
import { Login } from './pages/Login';
import { Home } from './pages/Home';
import { LoadingActions } from './pages/LoadingActions';
// import { LoadingGoodsUploadList } from './pages/LoadingGoodsUploadList/LoadingGoodsUploadList';
import { LoadingAddressList } from './pages/LoadingAddressList';
// import { LoadingFreightBillList } from './pages/LoadingFreightBillList';
// import { DeliveryActions } from './pages/DeliveryActions';
// import { DeliveryList } from './pages/DeliveryList';
// import { DeliveryUploadList } from './pages/DeliveryUploadList';
// import { DeliveryAddressList } from './pages/DeliveryAddressList';
// import { DeliveryFreightBillList } from './pages/DeliveryFreightBillList';
// import { ScanLoadingGoodsBarcode } from './pages/ScanLoadingGoodsBarcode';
function AuthRouter(props) {
    const { children } = props;
    const { token } = useToken();
    const isLogged = Boolean(token);
    return isLogged ? children : <Redirect to="/" />;
}

function NoAuthRoute(props) {
    const { token } = useToken();
    const isLogged = Boolean(token);
    return !isLogged ? <Route {...props} /> : <Redirect to="/home" />;
}

function DemoComp() {
    return (<div>DemoComp</div>)
}

function App() {
    return (
        <BrowserRouter>
            <Switch>
                <NoAuthRoute path="/" exact component={Login} />
                <AuthRouter>
                    <Switch>
                        <Route path="/home" exact component={Home} />
                        <Route path="/loading" exact component={LoadingActions} />
                        <Route path="/loading/address" exact component={LoadingAddressList} />
                        {/* <Route path="/loading/address/:address" exact component={LoadingFreightBillList} />
                        <Route path="/loading/address/:address/:freightBillNumber" exact component={ScanLoadingGoodsBarcode} />
                        <Route path="/loading/upload" exact component={LoadingGoodsUploadList} /> */}
                        {/* <Route path="/delivery" exact component={DeliveryActions} />
                        <Route path="/delivery/address" exact component={DeliveryAddressList} />
                        <Route path="/delivery/list/:address" exact component={DeliveryFreightBillList} />
                        <Route path="/delivery/list/:address/:freightBillNumber" exact component={DeliveryList} />
                        <Route path="/delivery/upload" exact component={DeliveryUploadList} /> */}
                        <Redirect to="/home" />
                    </Switch>
                </AuthRouter>
                <Redirect to="/" />
            </Switch>
        </BrowserRouter>


    );
}

export default App;
