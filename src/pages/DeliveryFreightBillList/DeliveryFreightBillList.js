import React, {
    useEffect,
    useState,
} from 'react';
import { useHistory, useParams } from 'react-router-dom';

import { Page } from '../../components/Page';
import { AppBar } from '../../components/AppBar';
import { SearchInput } from '../../components/Input/SearchInput/SearchInput';
import { Header } from '../../components/Header';
import { AddressList } from './components/AddressList';
import api from '../../api';
import { useLoading } from '../../utils/loading';
import { useToken } from '../../utils/storage/hooks/useToken';
import { ContinueButton } from '../DeliveryList/components/ContinueButton';
import { ReportDialog } from '../DeliveryList/components/ReportDialog';
import { ReportActionsDialog } from '../DeliveryList/components/ReportActionsDialog';
import { SignatureDialog } from '../../components/SignatureDialog';
import { SelectPhotosDialog } from '../../components/SelectPhotosDialog';
import { StatusCodeError } from '../../errors/StatusCodeError';
import { useAlert } from '../../utils/alert';
import Fab from '@material-ui/core/Fab';
import { SelectAllButton } from './components/SelectAllButton';

const addressListSelector = (addressList, filterText) => (
    addressList.filter((item) => filterText === '' || item.address.includes(filterText))
);

function DeliveryFreightBillList() {
    const history = useHistory();
    const { token } = useToken();
    const { address } = useParams();
    const { setIsLoading } = useLoading();
    const [searchText, setSearchText] = useState('');
    const [addressList, setAddressList] = useState([]);
    const customAlert = useAlert();
    const [reportDialogOpen, setReportDialogOpen] = useState(false);
    const [reportActionsDialogOpen, setReportActionsDialogOpen] = useState(false);
    const [signatureDialogOpen, setSignatureDialogOpen] = useState(false);
    const [selectPhotosDialogOpen, setSelectPhotosDialogOpen] = useState(false);
    const [collectOnDeliveryAmount, setCollectOnDeliveryAmount] = useState(0);
    const [dutyAmount, setDutyAmount] = useState(0);
    const [itemAmount, setItemAmount] = useState(0);
    const [isCollected, setIsCollected] = useState(false);
    const [isDutyCollected, setIsDutyCollected] = useState(false);
    const [exceptionOptions, setExceptionOptions] = useState([]);
    const [receiverName, setReceiverName] = useState('');

    useEffect(() => {
        loadData();
        loadExceptionReason();
    }, []);

    function loadData() {
        setIsLoading(true);
        api.admin.freightBill.getInTransitList(token)
            .then((freightBillGroupByAddressList) => {
                setIsLoading(false);
                const datas = [];
                if (freightBillGroupByAddressList[decodeURIComponent(address)]) {
                    freightBillGroupByAddressList[decodeURIComponent(address)].forEach((freightBill) => {
                        datas.push({
                            ...freightBill,
                            address: freightBill.receiverAddress,
                            freightBillNumber: freightBill.freightBillNumber,
                            freightBillId: freightBill.freightBillId,
                            itemAmount: freightBill.itemArray.length,
                            collectOnDeliveryAmount: !freightBill.isCodCollected ? freightBill.collectOnDelivery : 0,
                            dutyAmount: !freightBill.isDutyCollected ? freightBill.duty : 0,
                            checked: false,
                        });
                        setReceiverName(freightBill.receiverName);
                    });
                }

                setAddressList(datas);
            })
            .catch(async (error) => {
                setIsLoading(false);
                if (error instanceof StatusCodeError && error.response.status === 400) {
                    const { message } = await error.response.json();
                    alert(message);
                }
                console.error(error);
            });
    }

    function loadExceptionReason() {
        api.admin.exceptionReason.getAll()
            .then((exceptionReasonList) => {
                setExceptionOptions(
                    exceptionReasonList
                        .filter(byExceptionReasonTypeEqualDispatch)
                        .map(toExceptionOptions),
                );
            });

        function byExceptionReasonTypeEqualDispatch(exceptionReason) {
            return exceptionReason.exceptionReasonType === 'DISPATCH';
        }

        function toExceptionOptions(exceptionReason) {
            return {
                label: exceptionReason.name,
                value: exceptionReason.exceptionReasonId,
            };
        }
    }

    function handleSelect(freightBillId) {
        const newAddressList = addressList.map((item) => {
            if (item.freightBillId === freightBillId) {
                return {
                    ...item,
                    checked: !item.checked,
                };
            }

            return {
                ...item,
            };
        });
        setAddressList(newAddressList);
    }

    function handleSignatureComplete(signatureFile) {
        setSignatureDialogOpen(false);
        customAlert.show({
            type: 'waring',
            title: '確認',
            message: '確定要送出資料?',
            onConfirm: () => markAsDelivered([signatureFile]),
            cancelButton: true,
        });
    }

    function handleSelectPhotosComplete(photosFile) {
        setSelectPhotosDialogOpen(false);
        markAsDelivered(photosFile);
    }

    async function markAsDelivered(imageFiles) {
        setIsLoading(true);
        Promise.all(imageFiles.map((file) => api.file.uploadFile(file)))
            .then((fileIds) => {
                setIsLoading(false);
                const targetItemSerialNumberArray = addressList
                    .filter((address) => address.checked)
                    .reduce((serialNumbers, address) => {
                        address.itemArray.forEach((item) => {
                            serialNumbers.push(item.serialNumber);
                        });

                        return serialNumbers;
                    }, []);
                return api.admin.freightBill.markAsDelivered(targetItemSerialNumberArray, fileIds, isCollected, isDutyCollected);
            })
            .then(() => {
                setIsLoading(false);
                loadData();
            })
            .catch(async (error) => {
                setIsLoading(false);
                if (error instanceof StatusCodeError && error.response.status === 400) {
                    const { message } = await error.response.json();
                    alert(message);
                }
            });
    }

    function handleReportExceptionClick(selectedExceptionId) {
        if (!selectedExceptionId) {
            alert('請先選擇例外原因');
            return;
        }
        setReportDialogOpen(false);
        setIsLoading(true);

        const targetItemSerialNumberArray = addressList
            .filter((address) => address.checked)
            .reduce((serialNumbers, address) => {
                address.itemArray.forEach((item) => {
                    serialNumbers.push(item.serialNumber);
                });

                return serialNumbers;
            }, []);
        api.admin.freightBill.reportException(targetItemSerialNumberArray, selectedExceptionId)
            .then(() => {
                setIsLoading(false);
                loadData();
                alert('回報成功');
            })
            .catch(async (error) => {
                setIsLoading(false);
                if (error instanceof StatusCodeError && error.response.status === 400) {
                    const { message } = await error.response.json();
                    alert(message);
                }
            });
    }

    function handleContinueClick() {
        const selectedAddresses = addressList.filter((address) => address.checked);
        if (selectedAddresses.length === 0) {
            alert('請先選擇資料!');
            return;
        }

        let tempCollectOnDeliveryAmount = 0;
        let tempDutyAmount = 0;
        let tempItemAmount = 0;
        selectedAddresses
            .filter(bySelectedAddress)
            .forEach((freightBill) => {
                if (!freightBill.isCodCollected) {
                    tempCollectOnDeliveryAmount += parseInt(freightBill.collectOnDelivery, 10);
                }
                if (!freightBill.isDutyCollected) {
                    tempDutyAmount += parseInt(freightBill.duty, 10);
                }

                tempItemAmount += freightBill.itemArray.length;
            });
        setCollectOnDeliveryAmount(tempCollectOnDeliveryAmount);
        setDutyAmount(tempDutyAmount);
        setItemAmount(tempItemAmount);

        setReportDialogOpen(true);

        function bySelectedAddress(address) {
            return address.checked;
        }
    }

    function handleSelectAllClick() {
        setAddressList(addressList.map((address) => ({
            ...address,
            checked: true,
        })));
    }

    return (
        <Page title="派送作業 > 選擇票號">
            <AppBar title="派送作業 > 選擇票號" />
            <Header>
                <SearchInput value={searchText} onChange={(e) => setSearchText(e.target.value)} />
            </Header>
            <AddressList
                addressList={addressListSelector(addressList, searchText)}
                onItemClick={(freightBillNumber) => history.push(`/delivery/list/${encodeURIComponent(address)}/${freightBillNumber}`)}
                selectedFreightBillIds={[]}
                onSelect={handleSelect}
            />
            <ContinueButton onClick={handleContinueClick} />
            <ReportDialog
                open={reportDialogOpen}
                onBackdropClick={() => setReportDialogOpen(false)}
                onDeliveredClick={(isCollected, isDutyCollected) => {
                    setReportDialogOpen(false);
                    setReportActionsDialogOpen(true);
                    setIsCollected(isCollected);
                    setIsDutyCollected(isDutyCollected);
                }}
                onReportExceptionClick={handleReportExceptionClick}
                exceptionOptions={exceptionOptions}
                collectOnDeliveryAmount={collectOnDeliveryAmount}
                dutyAmount={dutyAmount}
            />
            <ReportActionsDialog
                open={reportActionsDialogOpen}
                onClose={() => setReportActionsDialogOpen(false)}
                onTakePhotoClick={() => {
                    setReportActionsDialogOpen(false);
                    setSelectPhotosDialogOpen(true);
                }}
                onSignatureClick={() => {
                    setReportActionsDialogOpen(false);
                    setSignatureDialogOpen(true);
                }}
            />
            <SignatureDialog
                open={signatureDialogOpen}
                onClose={() => setSignatureDialogOpen(false)}
                onComplete={handleSignatureComplete}
                itemAmount={itemAmount}
                collectOnDeliveryAmount={collectOnDeliveryAmount}
                dutyAmount={dutyAmount}
                isCollected={isCollected}
                isDutyCollected={isDutyCollected}
                receiverName={receiverName}
            />
            <SelectPhotosDialog
                open={selectPhotosDialogOpen}
                onClose={() => setSelectPhotosDialogOpen(false)}
                onComplete={handleSelectPhotosComplete}
            />
            <SelectAllButton onSelectAllClick={handleSelectAllClick} />
        </Page>
    );
}

export { DeliveryFreightBillList };
