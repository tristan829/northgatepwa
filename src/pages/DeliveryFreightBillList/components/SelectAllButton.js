import React from 'react';
import * as PropTypes from 'prop-types';
import Fab from '@material-ui/core/Fab';
import makeStyles from '@material-ui/core/styles/makeStyles';

SelectAllButton.propTypes = {
    onSelectAllClick: PropTypes.func.isRequired,
};

const useStyles = makeStyles((theme) => ({
    button: {
        position: 'absolute',
        right: theme.spacing(2),
        bottom: 56,
    },
}));

function SelectAllButton(props) {
    const { onSelectAllClick } = props;
    const classes = useStyles();
    return (
        <Fab className={classes.button} color="default" variant="round" onClick={onSelectAllClick}>
            全選
        </Fab>
    );
}

export { SelectAllButton };
