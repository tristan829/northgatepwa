import React from 'react';
import * as PropTypes from 'prop-types';

import makeStyles from '@material-ui/core/styles/makeStyles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import { List } from '../../../components/List';

AddressList.propTypes = {
    addressList: PropTypes.arrayOf(PropTypes.shape({
        address: PropTypes.string.isRequired,
        freightBillNumber: PropTypes.string.isRequired,
        freightBillId: PropTypes.string.isRequired,
        checked: PropTypes.bool.isRequired,
        itemAmount: PropTypes.number.isRequired,
        collectOnDeliveryAmount: PropTypes.number.isRequired,
        dutyAmount: PropTypes.number.isRequired,
    })).isRequired,
    onItemClick: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
};

const useStyles = makeStyles((theme) => ({
    card: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        margin: theme.spacing(1, 4),
        padding: theme.spacing(2),
    },
    row: {
        width: '100%',
        display: 'flex',
        padding: theme.spacing(1, 0),
    },
    col: {
        flex: 1,
    },
    content: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        marginLeft: theme.spacing(2),
        cursor: 'pointer',
    },
}));

function AddressList(props) {
    const {
        onItemClick,
        addressList,
        onSelect,
    } = props;
    const classes = useStyles();

    return (
        <List>
            {addressList.map((item) => (
                <Card className={classes.card}>
                    <Checkbox
                        defaultChecked
                        color="primary"
                        checked={item.checked}
                        onChange={() => onSelect(item.freightBillId)}
                    />
                    <div className={classes.content} onClick={() => onItemClick(item.freightBillNumber)}>
                        <Typography variant="h6" style={{ fontWeight: 'bold' }}>{item.address}</Typography>
                        <Typography variant="h6" style={{ fontWeight: 'bold' }}>{item.freightBillNumber}</Typography>
                        <div className={classes.row}>
                            <div className={classes.col}>
                                車上總件數: {item.itemAmount}
                            </div>
                        </div>
                        <div className={classes.row} style={{ color: '#dc3545' }}>
                            <div className={classes.col}>
                                代收 {item.collectOnDeliveryAmount} 元
                            </div>
                            <div className={classes.col}>
                                稅金 {item.dutyAmount} 元
                            </div>
                        </div>
                    </div>
                </Card>
            ))}
        </List>
    );
}

export { AddressList };
