import React, {
    useState,
} from 'react';

import { Page } from '../../components/Page/Page.js';
import { useStyles } from './style';
import { AccountInput } from './components/AccountInput';
import { PasswordInput } from './components/PasswordInput';
import { LoginButton } from './components/LoginButton';
import { Title } from './components/Title';
import { SubTitle } from './components/SubTitle';
import { useToken } from '../../utils/storage/hooks/useToken';
import { useLoading } from '../../utils/loading';
import api from '../../api';
import { StatusCodeError } from '../../errors/StatusCodeError';

function Login() {
    const classes = useStyles();
    const { setToken } = useToken();
    const { setIsLoading } = useLoading();
    const [account, setAccount] = useState('');
    const [password, setPassword] = useState('');

    function handleLoginClick() {
        setIsLoading(true);
        // 紹偉
        let fakeDriverId = '2c26da9d-311a-44bc-851c-7f95e38789b5'
        setIsLoading(false);
        //setToken(driverId);
        setToken(fakeDriverId);
        // api.admin.driver.login(account, password)
        //     .then(({ driverId }) => {
        //         //--- fetch Response
        //         // {
        //         //     "success": true,
        //         //     "driverId": "2c26da9d-311a-44bc-851c-7f95e38789b5"
        //         //   }
        //         //driverId = 2c26da9d-311a-44bc-851c-7f95e38789b5
        //         // 紹偉
        //         setIsLoading(false);
        //         setToken(driverId);
        //     })
        //     .catch(async (error) => {
        //         console.log('error--',error);
        //         setIsLoading(false);
        //         if (error instanceof StatusCodeError && error.response.status === 400) {
        //             const { message } = await error.response.json();
        //             alert(message);
        //         }
        //     });
    }

    return (
        <Page title="北門派件系統" className={classes.root}>
            
            <Title title="北門派件系統" />
            <SubTitle subTitle="登入" />
            <AccountInput value={account} onChange={(e) => setAccount(e.target.value)} />
            <PasswordInput value={password} onChange={(e) => setPassword(e.target.value)} />
            <LoginButton onClick={handleLoginClick} />
        </Page>
    );
}

export {Login};
