import React from 'react';
import * as PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import {
    faUser,
} from '@fortawesome/free-solid-svg-icons';

AccountInput.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
};

function AccountInput(props) {
    const {
        value,
        onChange,
    } = props;
    return (
        <TextField
            placeholder="帳號"
            variant="outlined"
            value={value}
            onChange={onChange}
            InputProps={{
                startAdornment: (
                    <InputAdornment position="start">
                        <Icon icon={faUser} />
                    </InputAdornment>
                ),
            }}
            fullWidth
        />
    );
}

export { AccountInput };
