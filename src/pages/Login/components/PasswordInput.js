import React from 'react';
import * as PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import {
    faLock,
} from '@fortawesome/free-solid-svg-icons';

PasswordInput.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
};

function PasswordInput(props) {
    const {
        value,
        onChange,
    } = props;
    return (
        <TextField
            placeholder="密碼"
            variant="outlined"
            value={value}
            onChange={onChange}
            type="password"
            InputProps={{
                startAdornment: (
                    <InputAdornment position="start">
                        <Icon icon={faLock} />
                    </InputAdornment>
                ),
            }}
            fullWidth
        />
    );
}

export { PasswordInput };
