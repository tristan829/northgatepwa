import React from 'react';
import * as PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

SubTitle.propTypes = {
    subTitle: PropTypes.string.isRequired,
};

function SubTitle(props) {
    const { subTitle } = props;

    return (
        <Typography variant="h5" style={{ margin: '16px 0' }}>{subTitle}</Typography>
    );
}

export { SubTitle };
