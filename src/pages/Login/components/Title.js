import React from 'react';
import * as PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

Title.propTypes = {
    title: PropTypes.string.isRequired,
};

function Title(props) {
    const { title } = props;
    return (
        <Typography variant="h4" style={{ fontWeight: 'bold', margin: '32px 0' }}>{title}</Typography>
    );
}

export { Title };
