import React from 'react';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

LoginButton.propTypes = {
    onClick: PropTypes.func.isRequired,
};

function LoginButton(props) {
    const { onClick } = props;
    return (
        <Button
            color="primary"
            variant="contained"
            size="large"
            onClick={onClick}
            fullWidth
            disableElevation
        >
            登入
        </Button>
    );
}

export { LoginButton };
