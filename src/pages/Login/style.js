import makeStyles from '@material-ui/core/styles/makeStyles';
import createStyles from '@material-ui/core/styles/createStyles';

export const useStyles = makeStyles((theme) => createStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(4),
        '& .MuiFormControl-root': {
            margin: theme.spacing(1, 0),
        },
        '& .MuiButton-root': {
            margin: theme.spacing(1, 0),
        },
    },
    subtitle: {
        margin: theme.spacing(2, 0),
    },
}));
