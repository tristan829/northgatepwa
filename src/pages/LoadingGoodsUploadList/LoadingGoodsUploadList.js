import React from 'react';
import { Page } from '../../components/Page';
import { AppBar } from '../../components/AppBar';
import { CustomerTable } from '../../components/CustomerTable';
import { UploadButton } from '../../components/Button/UploadButton';
import { Header } from '../../components/Header';
import { SearchInput } from '../../components/Input/SearchInput/SearchInput';
import { Footer } from '../../components/Footer';

function LoadingGoodsUploadList() {
    return (
        <Page title="分貨作業 > 未上傳列表">
            <AppBar title="分貨作業 > 未上傳列表" />
            <Header>
                <SearchInput value="" onChange={() => {}} />
            </Header>
            <CustomerTable data={[]} onItemClick={() => console.log('asdad')} />
            <Footer>
                <UploadButton onClick={() => {}} />
            </Footer>
        </Page>
    );
}

export { LoadingGoodsUploadList };
