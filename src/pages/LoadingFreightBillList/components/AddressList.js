import React from 'react';
import * as PropTypes from 'prop-types';

import makeStyles from '@material-ui/core/styles/makeStyles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';

import { List } from '../../../components/List';

AddressList.propTypes = {
    addressList: PropTypes.arrayOf(PropTypes.shape({
        address: PropTypes.string.isRequired,
        inCarAmount: PropTypes.number.isRequired,
        waitAmount: PropTypes.number.isRequired,
    })).isRequired,
    onItemClick: PropTypes.func.isRequired,
};

const useStyles = makeStyles((theme) => ({
    card: {
        display: 'flex',
        flexDirection: 'column',
        margin: theme.spacing(1, 4),
        padding: theme.spacing(2),
    },
    row: {
        display: 'flex',
        padding: theme.spacing(1, 0),
    },
    col: {
        flex: 1,
    },
}));

function AddressList(props) {
    const {
        onItemClick,
        addressList,
    } = props;
    const classes = useStyles();

    return (
        <List>
            {addressList.map((item) => (
                <Card className={classes.card} onClick={() => onItemClick(item.freightBillNumber)}>
                    <Typography variant="h6" style={{ fontWeight: 'bold' }}>{item.address}</Typography>
                    <Typography variant="h6" style={{ fontWeight: 'bold' }}>{item.freightBillNumber}</Typography>
                    <div className={classes.row}>
                        <div className={classes.col} style={{ color: '#28a745' }}>
                            已上車件數: {item.inCarAmount}
                        </div>
                        <div className={classes.col} style={{ color: '#dc3545' }}>
                            待上車總件數: {item.waitAmount}
                        </div>
                    </div>
                </Card>
            ))}
        </List>
    );
}

export { AddressList };
