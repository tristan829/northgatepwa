import React, {
    useEffect,
    useState,
} from 'react';
import { useHistory, useParams } from 'react-router-dom';

import { Page } from '../../components/Page';
import { AppBar } from '../../components/AppBar';
import { SearchInput } from '../../components/Input/SearchInput/SearchInput';
import { Header } from '../../components/Header';
import { AddressList } from './components/AddressList';
import { ScanBarcodeButton } from '../ScanLoadingGoodsBarcode/components/ScanBarcodeButton';
import api from '../../api';
import { useLoading } from '../../utils/loading';
import { useToken } from '../../utils/storage/hooks/useToken';

const addressListSelector = (addressList, filterText) => (
    addressList.filter((item) => filterText === '' || item.address.includes(filterText))
);

function LoadingFreightBillList() {
    const history = useHistory();
    const { token } = useToken();
    const { address } = useParams();
    const { setIsLoading } = useLoading();
    const [searchText, setSearchText] = useState('');
    const [addressList, setAddressList] = useState([]);

    useEffect(() => {
        setIsLoading(true);
        api.admin.freightBill.getWaitingForDriverToPickUpList(token)
            .then((freightBillGroupByAddressList) => {
                setIsLoading(false);
                setAddressList(freightBillGroupByAddressList[decodeURIComponent(address)].map((item) => ({
                    address: item.receiverAddress,
                    inCarAmount: item.numberOfPicked,
                    waitAmount: item.numberOfWaitingForPickUp,
                    freightBillNumber: item.freightBillNumber,
                })));
            })
            .catch((error) => {
                setIsLoading(false);
            });
    }, []);

    function handleBarcodeScanned(barcode) {
        history.push(`/loading/address/${address}/${encodeURIComponent(barcode)}`);
    }

    return (
        <Page title="分貨作業 > 選擇地址">
            <AppBar title="分貨作業 > 選擇地址" />
            <Header>
                <SearchInput value={searchText} onChange={(e) => setSearchText(e.target.value)} />
                {/*<ScanBarcodeButton onBarcodeScanned={handleBarcodeScanned} />*/}
            </Header>
            <AddressList
                addressList={addressListSelector(addressList, searchText)}
                onItemClick={(freightBillNumber) => history.push(`/loading/address/${address}/${freightBillNumber}`)}
            />
        </Page>
    );
}

export { LoadingFreightBillList };
