import React, {
    useState,
    useEffect,
} from 'react';
import {
    useParams,
    useHistory,
} from 'react-router-dom';

import { AppBar } from '../../components/AppBar';
import { Page } from '../../components/Page';
import { BarcodeInput } from './components/BarcodeInput';
import { ScanBarcodeButton } from './components/ScanBarcodeButton';
import { Header } from '../../components/Header';
import { CustomerTable } from '../../components/CustomerTable';
import { LoadingStatus } from './components/LoadingStatus';
import api from '../../api';
import { useLoading } from '../../utils/loading';
import { useToken } from '../../utils/storage/hooks/useToken';
import { StatusCodeError } from '../../errors/StatusCodeError';
import { useAlert } from '../../utils/alert';

function ScanLoadingGoodsBarcode() {
    const { token } = useToken();
    const history = useHistory();
    const alert = useAlert();
    const { setIsLoading } = useLoading();
    const { address, freightBillNumber } = useParams();
    const [data, setData] = useState([]);
    const [inCarAmount, setInCarAmount] = useState(0);
    const [amount, setAmount] = useState(0);

    useEffect(() => {
        loadData();
    }, []);

    function loadData() {
        setIsLoading(true);
        api.admin.freightBill.getWaitingForDriverToPickUpList(token)
            .then((freightBillGroupByAddressList) => {
                setIsLoading(false);
                const freightBill = freightBillGroupByAddressList[decodeURIComponent(address)].find((item) => item.freightBillNumber === decodeURIComponent(freightBillNumber));
                if (freightBill) {
                    setData(freightBill.itemArray.map((item) => ({
                        id: item.itemId,
                        inCar: item.deliverStatus === 'IN_TRANSIT',
                        customerName: freightBill.receiverName,
                        address: freightBill.receiverAddress,
                        phone: freightBill.receiverContactNumber,
                        serialNumber: item.serialNumber,
                    })));
                    setInCarAmount(freightBill.numberOfPicked);
                    setAmount(freightBill.itemArray.length);
                }
            })
            .catch(async (error) => {
                setIsLoading(false);
                if (error instanceof StatusCodeError && error.response.status === 400) {
                    const { message } = await error.response.json();
                    alert.show({
                        type: 'error',
                        title: '錯誤',
                        message,
                    });
                } else {
                    history.goBack();
                }
                console.error('error', error);
            });
    }

    function handleBarcodeScanned(barcode) {
        setIsLoading(true);
        api.admin.freightBill.markAsDriverPicked([barcode], token)
            .then(() => {
                setIsLoading(false);
                alert.show({
                    type: 'success',
                    title: '成功',
                    message: `${barcode} 已成功上車`,
                    canceledOnTouchOutside: true,
                });
                loadData();
            })
            .catch(async (error) => {
                setIsLoading(false);
                if (error instanceof StatusCodeError && error.response.status === 400) {
                    const { message } = await error.response.json();
                    alert.show({
                        type: 'error',
                        title: '錯誤',
                        message,
                    });
                }
                console.error('error', error);
            });
    }

    return (
        <Page title="分貨作業 > 掃描貨件條碼">
            <AppBar title="分貨作業 > 掃描貨件條碼" />
            <Header>
                <BarcodeInput />
                <ScanBarcodeButton
                    onBarcodeScanned={handleBarcodeScanned}
                />
            </Header>
            <CustomerTable data={data} freightBillNumber={freightBillNumber} />
            <LoadingStatus inCarAmount={inCarAmount} amount={amount} />
        </Page>
    );
}

export { ScanLoadingGoodsBarcode };
