import React from 'react';
import * as PropTypes from 'prop-types';

LoadingStatus.propTypes = {
    amount: PropTypes.number.isRequired,
    inCarAmount: PropTypes.number.isRequired,
};

function LoadingStatus(props) {
    const {
        amount,
        inCarAmount,
    } = props;

    return (
        <div
            style={{
                padding: 8,
                backgroundColor: '#6c757d',
                textAlign: 'center',
                fontSize: 22,
                fontWeight: 'bold',
                color: '#FFFFFF',
            }}
        >
            {inCarAmount}/{amount}
        </div>
    );
}

export { LoadingStatus };
