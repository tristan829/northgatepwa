import React from 'react';
import * as PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

BarcodeInput.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.string.isRequired,
};

function BarcodeInput(props) {
    const {
        value,
        onChange,
    } = props;

    return (
        <TextField
            placeholder="顯示/手動輸入條碼"
            variant="outlined"
            value={value}
            onChange={onChange}
            fullWidth
        />
    );
}

export { BarcodeInput };
