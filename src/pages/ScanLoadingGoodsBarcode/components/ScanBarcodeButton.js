import React, {
    useEffect,
} from 'react';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { faQrcode } from '@fortawesome/fontawesome-free';

ScanBarcodeButton.propTypes = {
    onBarcodeScanned: PropTypes.func,
};

ScanBarcodeButton.defaultProps = {
    onBarcodeScanned: (data) => {},
};

const useBarcodeScan = (onScanned) => {
    useEffect(() => {
        document.addEventListener('message', handleMessage);

        function handleMessage(e) {
            if (e.data && typeof e.data === 'string') {
                const data = JSON.parse(e.data);
                if (data.type === 'BARCODE_SCAN') {
                    onScanned(data.data);
                }
            }
        }

        return () => {
            document.removeEventListener('message', handleMessage);
        };
    }, []);

    const scanBarcode = () => {
        if (window.ReactNativeWebView?.postMessage) {
            window.ReactNativeWebView.postMessage(JSON.stringify({
                type: 'BARCODE_SCAN',
            }));
        }
    };

    return { scanBarcode };
};

function ScanBarcodeButton(props) {
    const { onBarcodeScanned } = props;
    const { scanBarcode } = useBarcodeScan(onBarcodeScanned);

    return (
        <Button
            variant="contained"
            color="primary"
            size="large"
            startIcon={<Icon icon={faQrcode} />}
            disableElevation
            fullWidth
            onClick={() => scanBarcode()}
        >
            掃描條碼
        </Button>
    );
}

export { ScanBarcodeButton };
