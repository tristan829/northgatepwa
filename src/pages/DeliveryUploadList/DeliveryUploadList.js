import React from 'react';
import { Page } from '../../components/Page';
import { AppBar } from '../../components/AppBar';
import { CustomerTable } from '../../components/CustomerTable';
import { UploadButton } from '../../components/Button/UploadButton/UploadButton';
import { Header } from '../../components/Header';
import { SearchInput } from '../../components/Input/SearchInput/SearchInput';
import { Footer } from '../../components/Footer';

function DeliveryUploadList() {
    return (
        <Page title="待派貨件 > 未上傳列表">
            <AppBar title="待派貨件 > 未上傳列表" />
            <Header>
                <SearchInput value="" onChange={() => {}} />
            </Header>
            <CustomerTable data={[]} />
            <Footer>
                <UploadButton onClick={() => {}} />
            </Footer>
        </Page>
    );
}

export { DeliveryUploadList };
