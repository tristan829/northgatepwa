import React from 'react';
import * as PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import Button from '@material-ui/core/Button';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { faCamera, faFileSignature } from '@fortawesome/fontawesome-free';
import Dialog from '@material-ui/core/Dialog/Dialog';

ReportActionsDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onTakePhotoClick: PropTypes.func.isRequired,
    onSignatureClick: PropTypes.func.isRequired,
};

function ReportActionsDialog(props) {
    const {
        open,
        onClose,
        onTakePhotoClick,
        onSignatureClick,
    } = props;

    return (
        <Dialog
            open={open}
            fullWidth
            maxWidth="xs"
        >
            <DialogTitle style={{ textAlign: 'center', borderBottom: '1px solid #DDDDDD', marginBottom: 8 }}>
                選擇動作
            </DialogTitle>
            <DialogContent>
                <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                    <Button
                        variant="contained"
                        color="primary"
                        size="large"
                        disableElevation
                        onClick={onSignatureClick}
                    >
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',
                                padding: '8px 0',
                            }}
                        >
                            <Icon icon={faFileSignature} size="3x" fixedWidth />
                            <span style={{ marginTop: 8 }}>客戶簽收</span>
                        </div>
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        size="large"
                        disableElevation
                        onClick={onTakePhotoClick}
                    >
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',
                                padding: '8px 0',
                            }}
                        >
                            <Icon icon={faCamera} size="3x" fixedWidth />
                            <span style={{ marginTop: 8 }}>照片</span>
                        </div>
                    </Button>
                </div>
            </DialogContent>
            <Button
                variant="contained"
                size="large"
                disableElevation
                fullWidth
                style={{
                    backgroundColor: '#F2F2F2',
                    marginTop: 8,
                    borderRadius: 0,
                    color: '#555555',
                }}
                onClick={onClose}
            >
                取消
            </Button>
        </Dialog>
    );
}

export { ReportActionsDialog };
