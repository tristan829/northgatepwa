import React from 'react';
import * as PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox';

import {
    List,
    ListHeader,
    ListItem,
    ListCell,
    ListContainer,
} from '../../../../components/List';

CustomerTable.propTypes = {
    freightBillNumber: PropTypes.string.isRequired,
    data: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        serialNumber: PropTypes.string.isRequired,
        customerName: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired,
        selected: PropTypes.bool.isRequired,
    })).isRequired,
    onItemClick: PropTypes.func,
};

CustomerTable.defaultProps = {
    onItemClick: () => {},
};

function CustomerTable(props) {
    const { data, onItemClick, freightBillNumber } = props;

    return (
        <ListContainer>
            <ListHeader>
                <ListCell width={160}>序號</ListCell>
                <ListCell width={160}>提單號碼</ListCell>
                <ListCell width={120}>客戶名稱</ListCell>
                <ListCell width={180}>電話</ListCell>
                <ListCell width={280}>地址</ListCell>
            </ListHeader>
            <List>
                {data.map((item) => (
                    <ListItem onClick={() => onItemClick(item.serialNumber)}>
                        <ListCell width={160}>{item.serialNumber}</ListCell>
                        <ListCell width={160}>{freightBillNumber}</ListCell>
                        <ListCell width={120}>{item.customerName}</ListCell>
                        <ListCell width={180}>{item.phone}</ListCell>
                        <ListCell width={280}>{item.address}</ListCell>
                    </ListItem>
                ))}
            </List>
        </ListContainer>
    );
}

export { CustomerTable };
