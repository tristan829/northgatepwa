import React, {
    useState,
} from 'react';
import * as PropTypes from 'prop-types';

import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Dialog from '@material-ui/core/Dialog';

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { faBoxCheck, faShare } from '@fortawesome/fontawesome-free';

ReportDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onBackdropClick: PropTypes.func,
    onDeliveredClick: PropTypes.func,
    onReportExceptionClick: PropTypes.func,
    exceptionOptions: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
    })),
    collectOnDeliveryAmount: PropTypes.number.isRequired,
    dutyAmount: PropTypes.number.isRequired,
};

ReportDialog.defaultProps = {
    onBackdropClick: () => {},
    onDeliveredClick: () => {},
    onReportExceptionClick: (selectedExceptionValue) => {},
    exceptionOptions: [],
};

function ReportDialog(props) {
    const {
        open,
        onBackdropClick,
        onDeliveredClick,
        onReportExceptionClick,
        exceptionOptions,
        collectOnDeliveryAmount,
        dutyAmount,
    } = props;
    const [selectedExceptionValue, setSelectedExceptionValue] = useState('');
    const [isCollected, setIsCollected] = useState(1);
    const [isDutyCollected, setIsDutyCollected] = useState(1);

    function handleReportExceptionClick() {
        onReportExceptionClick(selectedExceptionValue);
    }

    function handleDeliveredClick() {
        onDeliveredClick(isCollected, isDutyCollected);
    }

    return (
        <Dialog
            open={open}
            fullWidth
            maxWidth="xs"
            onBackdropClick={onBackdropClick}
        >
            <DialogContent>
                <Button
                    variant="contained"
                    color="primary"
                    size="large"
                    disableElevation
                    fullWidth
                    startIcon={(<Icon icon={faBoxCheck} />)}
                    onClick={handleDeliveredClick}
                >
                    已送達
                </Button>
                <div style={{ display: 'flex', margin: '16px 0', justifyContent: 'space-between' }}>
                    <div>代收 {collectOnDeliveryAmount} 元</div>
                    <div>稅金 {dutyAmount} 元</div>
                </div>
                <div style={{ display: 'flex', margin: '16px 0' }}>
                    <FormControl variant="outlined" style={{ flex: 1, marginRight: 8 }}>
                        <InputLabel>例外狀況</InputLabel>
                        <Select
                            label="例外狀況"
                            value={selectedExceptionValue}
                            onChange={(e) => setSelectedExceptionValue(e.target.value)}
                        >
                            {exceptionOptions.map((exception) => (
                                <MenuItem value={exception.value}>{exception.label}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <Button
                        variant="contained"
                        color="secondary"
                        size="large"
                        disableElevation
                        startIcon={(<Icon icon={faShare} />)}
                        onClick={handleReportExceptionClick}
                    >
                        回報
                    </Button>
                </div>
            </DialogContent>
        </Dialog>
    );
}

export { ReportDialog };
