import React from 'react';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

ContinueButton.propTypes = {
    onClick: PropTypes.func.isRequired,
};

function ContinueButton(props) {
    const { onClick } = props;

    return (
        <Button
            variant="contained"
            color="primary"
            size="large"
            style={{ borderRadius: 0 }}
            disableElevation
            fullWidth
            onClick={onClick}
        >
            繼續
        </Button>
    );
}

export { ContinueButton };
