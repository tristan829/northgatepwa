import React, { useEffect, useState } from 'react';
import {
    useParams,
} from 'react-router-dom';

import { Page } from '../../components/Page';
import { AppBar } from '../../components/AppBar';
import { CustomerTable } from './components/CustomerTable';
import { SearchInput } from '../../components/Input/SearchInput/SearchInput';
import { Header } from '../../components/Header';
import api from '../../api';
import { StatusCodeError } from '../../errors/StatusCodeError';
import { useToken } from '../../utils/storage/hooks/useToken';
import { useLoading } from '../../utils/loading';

function DeliveryList() {
    const { token } = useToken();
    const { setIsLoading } = useLoading();
    const { address, freightBillNumber } = useParams();
    const [itemArray, setItemArray] = useState([]);

    useEffect(() => {
        loadData();
    }, []);

    function loadData() {
        setIsLoading(true);
        api.admin.freightBill.getInTransitList(token)
            .then((freightBillGroupByAddressList) => {
                setIsLoading(false);
                const datas = [];
                freightBillGroupByAddressList[decodeURIComponent(address)].forEach((freightBill) => {
                    if (freightBillNumber === freightBill.freightBillNumber) {
                        datas.push(...(freightBill.itemArray.map((item) => ({
                            id: item.serialNumber,
                            serialNumber: item.serialNumber,
                            inCar: item.deliverStatus === 'IN_TRANSIT',
                            customerName: freightBill.receiverName,
                            address: freightBill.receiverAddress,
                            phone: freightBill.receiverContactNumber,
                            selected: false,
                        }))));
                    }
                });

                setItemArray(datas);
            })
            .catch(async (error) => {
                setIsLoading(false);
                if (error instanceof StatusCodeError && error.response.status === 400) {
                    const { message } = await error.response.json();
                    alert(message);
                }
                console.error(error);
            });
    }

    return (
        <Page title="待派貨件 > 待派貨件列表">
            <AppBar title="待派貨件 > 待派貨件列表" />
            <Header>
                <SearchInput value="" onChange={() => {}} />
            </Header>
            <CustomerTable
                data={itemArray}
                freightBillNumber={freightBillNumber}
                onItemClick={() => {}}
                selectedSerialNumber={[]}
            />
        </Page>
    );
}

export { DeliveryList };
