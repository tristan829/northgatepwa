import React from 'react';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import {
    faScanner,
    faList,
} from '@fortawesome/fontawesome-free';

import { Page } from '../../components/Page';
import { Menu } from '../../components/Menu/Menu';
import { MenuItem } from '../../components/Menu/MenuItem';
import { AppBar } from '../../components/AppBar';

function LoadingActions() {
    const history = useHistory();

    return (
        <Page title="分貨作業">
            <AppBar title="分貨作業" />
            <Menu>
                <MenuItem
                    icon={<Icon icon={faScanner} fixedWidth />}
                    text="掃描貨件條碼"
                    onClick={() => history.push('/loading/address')}
                />
                <MenuItem
                    icon={<Icon icon={faList} fixedWidth />}
                    text="未上傳列表"
                    onClick={() => history.push('/loading/upload')}
                />
            </Menu>
        </Page>
    );
}

export { LoadingActions };
