import React from 'react';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import {
    faScanner,
    faList,
} from '@fortawesome/fontawesome-free';

import { Page } from '../../components/Page';
import { Menu } from '../../components/Menu/Menu';
import { MenuItem } from '../../components/Menu/MenuItem';
import { AppBar } from '../../components/AppBar';

function DeliveryActions() {
    const history = useHistory();

    return (
        <Page title="派送作業">
            <AppBar title="派送作業" />
            <Menu>
                <MenuItem
                    icon={<Icon icon={faScanner} fixedWidth />}
                    text="派送作業列表"
                    onClick={() => history.push('/delivery/address')}
                />
                <MenuItem
                    icon={<Icon icon={faList} fixedWidth />}
                    text="未上傳列表"
                    onClick={() => history.push('/delivery/upload')}
                />
            </Menu>
        </Page>
    );
}

export { DeliveryActions };
