import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { Page } from '../../components/Page';
import { AppBar } from '../../components/AppBar';
import { SearchInput } from '../../components/Input/SearchInput/SearchInput';
import { Header } from '../../components/Header';
import { AddressList } from './components/AddressList';
import { ScanBarcodeButton } from '../ScanLoadingGoodsBarcode/components/ScanBarcodeButton';
import { useLoading } from '../../utils/loading';
import api from '../../api';
import { useToken } from '../../utils/storage/hooks/useToken';
import { StatusCodeError } from '../../errors/StatusCodeError';

const addressListSelector = (addressList, filterText) => (
    addressList.filter((item) => filterText === '' || item.address.includes(filterText))
);

function DeliveryAddressList() {
    const { token } = useToken();
    const history = useHistory();
    const { setIsLoading } = useLoading();
    const [searchText, setSearchText] = useState('');
    const [addressList, setAddressList] = useState([]);

    useEffect(() => {
        setIsLoading(true);
        api.admin.freightBill.getInTransitList(token)
            .then((freightBillGroupByAddressList) => {
                setIsLoading(false);
                const freightBillList = Object.keys(freightBillGroupByAddressList)
                    .map((address) => {
                        const freightBillGroup = freightBillGroupByAddressList[address];
                        const itemAmount = freightBillGroup.reduce((attr, freightBill) => (
                            parseInt(attr, 10) + parseInt(freightBill.itemArray.length, 10)
                        ), 0);
                        const collectOnDeliveryAmount = freightBillGroup.reduce((attr, freightBill) => {
                            if (!freightBill.isCodCollected) {
                                return parseInt(attr, 10) + parseInt(freightBill.collectOnDelivery, 10);
                            }

                            return parseInt(attr, 10);
                        }, 0);
                        const dutyAmount = freightBillGroup.reduce((attr, freightBill) => {
                            if (!freightBill.isDutyCollected) {
                                return parseInt(attr, 10) + parseInt(freightBill.duty, 10);
                            }

                            return parseInt(attr, 10);
                        }, 0);

                        let itemLastStatusUpdateTime = 0;
                        if (freightBillGroup.length > 0) {
                            itemLastStatusUpdateTime = freightBillGroup[0].itemLastStatusUpdateTime;
                        }
                        const receiverContactNumber = freightBillGroup.length > 0 ? freightBillGroup[0].receiverContactNumber : '';

                        return {
                            address,
                            inCarAmount: itemAmount,
                            collectOnDeliveryAmount,
                            dutyAmount,
                            itemLastStatusUpdateTime,
                            receiverContactNumber,
                        };
                    }).sort((a, b) => b.itemLastStatusUpdateTime - a.itemLastStatusUpdateTime);
                setAddressList(freightBillList);
            })
            .catch(async (error) => {
                setIsLoading(false);
                if (error instanceof StatusCodeError && error.response.status === 400) {
                    const { message } = await error.response.json();
                    alert(message);
                }
            });
    }, []);

    function handleBarcodeScanned(address) {
        history.push(`/delivery/list/${encodeURIComponent(address)}`);
    }

    return (
        <Page title="派送作業 > 選擇地址">
            <AppBar title="派送作業 > 選擇地址" />
            <Header>
                <SearchInput value={searchText} onChange={(e) => setSearchText(e.target.value)} />
                <ScanBarcodeButton onBarcodeScanned={handleBarcodeScanned} />
            </Header>
            <AddressList
                addressList={addressListSelector(addressList, searchText)}
                onItemClick={(address) => history.push(`/delivery/list/${encodeURIComponent(address)}`)}
            />
        </Page>
    );
}

export { DeliveryAddressList };
