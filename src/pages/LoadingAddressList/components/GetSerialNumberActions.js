import React, { useState } from 'react';
import * as PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import makeStyles from '@material-ui/core/styles/makeStyles';

import { ScanBarcodeButton } from '../../ScanLoadingGoodsBarcode/components/ScanBarcodeButton';
import { SerialNumberInputDialog } from '../../../components/SerialNumberInputDialog/SerialNumberInputDialog';

GetSerialNumberActions.propTypes = {
    onGetSerialNumber: PropTypes.func.isRequired,
};

const useStyles = makeStyles((theme) => ({
    row: {
        display: 'flex',
    },
    button: {
        whiteSpace: 'nowrap',
        marginLeft: theme.spacing(1),
    },
}));

function GetSerialNumberActions(props) {
    const {
        onGetSerialNumber,
    } = props;
    const classes = useStyles();
    const [serialNumberInputDialogOpen, setSerialNumberInputDialogOpen] = useState();

    return (
        <div className={classes.row}>
            <ScanBarcodeButton onBarcodeScanned={onGetSerialNumber} />
            <Button
                className={classes.button}
                variant="contained"
                color="secondary"
                disableElevation
                onClick={() => setSerialNumberInputDialogOpen(true)}
            >
                手動輸入
            </Button>
            <SerialNumberInputDialog
                open={serialNumberInputDialogOpen}
                onClose={() => setSerialNumberInputDialogOpen(false)}
                onComplete={(serialNumber) => {
                    onGetSerialNumber(serialNumber);
                    setSerialNumberInputDialogOpen(false);
                }}
            />
        </div>
    );
}

export { GetSerialNumberActions };
