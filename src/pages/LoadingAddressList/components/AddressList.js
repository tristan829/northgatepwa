import React from 'react';
import * as PropTypes from 'prop-types';

import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';

import { List } from '../../../components/List';

AddressList.propTypes = {
    addressList: PropTypes.arrayOf(PropTypes.shape({
        address: PropTypes.string.isRequired,
        ticketAmount: PropTypes.number.isRequired,
        itemAmount: PropTypes.number.isRequired,
        onCarAmount: PropTypes.number.isRequired,
        waitAmount: PropTypes.number.isRequired,
    })).isRequired,
    onItemClick: PropTypes.func.isRequired,
};

const useStyles = makeStyles((theme) => ({
    list: {
        width: '100%',
    },
    card: {
        display: 'flex',
        flexDirection: 'column',
        margin: theme.spacing(1, 4),
        padding: theme.spacing(2),
    },
    row: {
        display: 'flex',
        padding: theme.spacing(1, 0),
        width: '100%',
        overflowX: 'hidden',
    },
    col: {
        flex: 1,
    },
}));

function AddressList(props) {
    const {
        onItemClick,
        addressList,
        onCarAmount,
        waitAmount,
    } = props;
    const classes = useStyles();

    return (
        <List className={classes.list}>
            {addressList.map((item) => (
                <Card className={classes.card} onClick={() => onItemClick(item.address)}>
                    <Typography variant="h6" style={{ fontWeight: 'bold' }}>{item.address}</Typography>
                    <div className={classes.row}>
                        <a href={`tel:${item.receiverContactNumber}`} onClick={(e) => e.stopPropagation()}>
                            <Typography variant="h6">{item.receiverContactNumber}</Typography>
                        </a>
                    </div>
                    <div className={classes.row}>
                        <div className={classes.col}>
                            共: {item.ticketAmount}票
                        </div>
                        <div className={classes.col} style={{ color: '#dc3545' }}>
                            待上車件數: {item.waitAmount}
                        </div>
                        <div className={classes.col} style={{ color: '#28a745' }}>
                            已車上件數: {item.onCarAmount}
                        </div>
                    </div>
                </Card>
            ))}
        </List>
    );
}

export { AddressList };
