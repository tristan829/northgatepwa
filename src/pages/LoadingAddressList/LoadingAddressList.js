import React, {
    useEffect,
    useState,
} from 'react';
import { useHistory } from 'react-router-dom';

import { Page } from '../../components/Page';
import { AppBar } from '../../components/AppBar';
import { SearchInput } from '../../components/Input/SearchInput/SearchInput';
import { Header } from '../../components/Header';
import { AddressList } from './components/AddressList';
// import { ScanBarcodeButton } from '../ScanLoadingGoodsBarcode/components/ScanBarcodeButton';
import api from '../../api';
import { useLoading } from '../../utils/loading';
import { useToken } from '../../utils/storage/hooks/useToken';
import { StatusCodeError } from '../../errors/StatusCodeError';
import { useAlert } from '../../utils/alert';
import { useToast } from '../../utils/toast';
import { GetSerialNumberActions } from './components/GetSerialNumberActions';

const addressListSelector = (addressList, filterText) => (
    addressList.filter((item) => filterText === '' || item.address.includes(filterText))
);

function LoadingAddressList() {
    const history = useHistory();
    const { token } = useToken();
    const { setIsLoading } = useLoading();
    const { showSuccessToast } = useToast();
    const alert = useAlert();
    const [searchText, setSearchText] = useState('');
    const [addressList, setAddressList] = useState([]);

    useEffect(() => {
        loadData();
    }, []);

    function loadData() {
        let freight1 = {
            address:"台北市忠孝東路112號",
            itemAmount:5,
            onCarAmount:6,
            waitAmount:3,
            ticketAmount: 7,//freightBillGroup.length,
            receiverContactNumber:"0938-256-321",
        }
        let freight2 = {
            address:"台北市忠孝東路112號",
            itemAmount:5,
            onCarAmount:6,
            waitAmount:3,
            ticketAmount: 7,//freightBillGroup.length,
            receiverContactNumber:"0938-256-321",
        }
        let freightBillList = []
        freightBillList.push(freight1)
        freightBillList.push(freight2)
        setAddressList(freightBillList);
        // return new Promise(((resolve, reject) => {
        //     setIsLoading(true);
        //     api.admin.freightBill.getWaitingForDriverToPickUpList(token)
        //         .then((freightBillGroupByAddressList) => {
        //             setIsLoading(false);
        //             const freightBillList = Object.keys(freightBillGroupByAddressList)
        //                 .map((address) => {
        //                     const freightBillGroup = freightBillGroupByAddressList[address];
        //                     const onCarAmount = freightBillGroup.reduce((attr, item) => {
        //                         return parseInt(attr, 10) + parseInt(item.itemArray.filter((i) => i.deliverStatus === 'IN_TRANSIT').length, 0);
        //                     }, 0);
        //                     const waitAmount = freightBillGroup.reduce((attr, item) => {
        //                         return parseInt(attr, 10) + parseInt(item.itemArray.filter((i) => i.deliverStatus === 'CHECKED').length, 0);
        //                     }, 0);
        //                     const itemAmount = freightBillGroup.reduce((attr, freightBill) => (
        //                         parseInt(attr, 10) + parseInt(freightBill.itemArray.length, 10)
        //                     ), 0);
        //                     const receiverContactNumber = freightBillGroup.length > 0 ? freightBillGroup[0].receiverContactNumber : '';
        //                     return {
        //                         address,
        //                         itemAmount,
        //                         onCarAmount,
        //                         waitAmount,
        //                         ticketAmount: freightBillGroup.length,
        //                         receiverContactNumber,
        //                     };
        //                 });

        //             setAddressList(freightBillList);
        //             resolve();
        //         })
        //         .catch((error) => {
        //             setIsLoading(false);
        //             reject(error);
        //         });
        // }));
    }

    function handleBarcodeScanned(barcode) {
        const serialNumber = barcode;
        alert(serialNumber)
        // setIsLoading(true);
        // api.admin.freightBill.markAsDriverPicked([serialNumber], token)
        //     .then(() => {
        //         setIsLoading(false);
        //         loadData()
        //             .then(() => {
        //                 alert.show({
        //                     type: 'success',
        //                     title: '成功',
        //                     message: `貨件序號: ${serialNumber} 掃描成功`,
        //                     canceledOnTouchOutside: true,
        //                 });
        //             });
        //     })
        //     .catch(async (error) => {
        //         setIsLoading(false);
        //         if (error instanceof StatusCodeError && error.response.status === 400) {
        //             const { message } = await error.response.json();
        //             if (window.navigator.vibrate) {
        //                 navigator.vibrate(500);
        //             }
        //             alert.show({
        //                 type: 'error',
        //                 title: '錯誤',
        //                 message,
        //             });
        //         }
        //         console.error('error', error);
        //     });
    }

    return (
        <Page title="分貨作業 > 選擇地址">
            <AppBar title="分貨作業 > 選擇地址" />
            <Header>
                <SearchInput value={searchText} onChange={(e) => setSearchText(e.target.value)} />
                <GetSerialNumberActions onGetSerialNumber={handleBarcodeScanned} />
            </Header>
            <AddressList
                addressList={addressListSelector(addressList, searchText)}
                onItemClick={(freightBillNumber) => history.push(`/loading/address/${freightBillNumber}`)}
            />
        </Page>
    );
}

export { LoadingAddressList };
