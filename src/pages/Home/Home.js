import React, { useEffect, useState } from 'react';
import {
    useHistory,
} from 'react-router-dom';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
// import {
//     faScannerKeyboard,
//     faList,
//     faSignOut,
// } from '@fortawesome/free-solid-svg-icons';
import {faList, faBarcode, faSignOutAlt} from '@fortawesome/free-solid-svg-icons';
// import { } from '@fortawesome/fontawesome-free';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';



import { Page } from '../../components/Page';
import { AppBar } from '../../components/AppBar';
import { Menu } from '../../components/Menu/Menu';
import { MenuItem } from '../../components/Menu/MenuItem';
import { useToken } from '../../utils/storage/hooks/useToken';
// import api from '../../api';

function Home() {
    const history = useHistory();
    const { token, clearToken } = useToken();
    const [driverName, setDriverName] = useState('');

    useEffect(() => {
        // api.admin.driver.getAll()
        //     .then((driverList) => {
        //         const driver = driverList.find((d) => d.driverId === token);
        //         if (driver) {
        //             setDriverName(driver.name);
        //         }
        //     });
    }, []);

    return (
        <Page title="北門派件系統">
            <div>AAA</div>
            <AppBar title="北門派件系統" />
            <Menu>
                <MenuItem
                    icon={<Icon icon={faBarcode} fixedWidth />}
                    text="彰化分貨作業"
                    onClick={() => history.push('/loading/address')}
                />
                <MenuItem
                    icon={<Icon icon={faList} fixedWidth />}
                    text="派送作業"
                    onClick={() => history.push('/delivery/address')}
                />
                <MenuItem
                    icon={<Icon icon={faSignOutAlt} fixedWidth />}
                    text="登出"
                    onClick={() => clearToken()}
                />
                <Box p={1}>
                    <Typography variant="h4">司機: {driverName}</Typography>
                </Box>
            </Menu>
        </Page>
    );
}

export { Home };
