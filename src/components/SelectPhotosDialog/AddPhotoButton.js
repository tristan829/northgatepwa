import React, {
    useRef,
} from 'react';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

AddPhotoButton.propTypes = {
    onPhotoSelected: PropTypes.func,
};

AddPhotoButton.defaultProps = {
    onPhotoSelected: () => {},
};

function AddPhotoButton(props) {
    const { onPhotoSelected } = props;
    const fileInput = useRef(null);

    function handleAddClick() {
        fileInput.current.click();
    }

    function handleFileInputChange(e) {
        onPhotoSelected(e.target.files[0]);
    }

    return (
        <div style={{ marginBottom: 8 }}>
            <Button
                variant="contained"
                size="large"
                color="primary"
                disableElevation
                fullWidth
                onClick={handleAddClick}
            >
                新增相片
            </Button>
            <form
                encType="multipart/form-data"
                style={{ display: 'none' }}
            >
                <input
                    type="file"
                    accept="image/*"
                    ref={fileInput}
                    onChange={handleFileInputChange}
                />
            </form>
        </div>
    );
}

export { AddPhotoButton };
