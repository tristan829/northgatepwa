import React, {
    useState,
} from 'react';
import * as PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog/Dialog';
import { useStyles } from './style';
import { List, ListItem } from '../List';
import { AddPhotoButton } from './AddPhotoButton';

SelectPhotosDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onComplete: PropTypes.func,
};

SelectPhotosDialog.defaultProps = {
    onComplete: (photoFiles) => {},
};

function dataURLtoFile(dataurl, filename) {

    let arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);

    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
}

function SelectPhotosDialog(props) {
    const {
        open,
        onClose,
        onComplete,
    } = props;
    const classes = useStyles();
    const [photoFiles, setPhotoFiles] = useState([]);
    const [photoFileDataUrls, setPhotoFileDataUrls] = useState([]);

    function handlePhotoSelected(file) {
        convertFile(file)
            .then((base64) => {
                setPhotoFiles([...photoFiles, file]);
                setPhotoFileDataUrls([...photoFileDataUrls, base64]);
            });
    }

    function handleRemoveItemClick(index) {
        setPhotoFiles(photoFiles.filter((value, i) => i !== index));
        setPhotoFileDataUrls(photoFileDataUrls.filter((value, i) => i !== index));
    }

    function handleCancelClick() {
        onClose();
    }

    function handleCompleteClick() {
        onComplete(photoFiles);
    }

    function convertFile(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = () => { resolve(reader.result); };
            reader.onerror = () => { reject(reader.error); };
            reader.readAsDataURL(file);
        });
    }

    return (
        <Dialog
            open={open}
            fullWidth
            maxWidth="xs"
        >
            <DialogTitle style={{ textAlign: 'center', borderBottom: '1px solid #DDDDDD', marginBottom: 8 }}>
                照片
            </DialogTitle>
            <DialogContent>
                <AddPhotoButton onPhotoSelected={handlePhotoSelected} />
                <List>
                    {photoFileDataUrls.map((src, index) => (
                        <ListItem className={classes.photoListItem}>
                            <img className={classes.photoListItemImg} src={src} />
                            <Button
                                variant="contained"
                                size="large"
                                color="secondary"
                                disableElevation
                                onClick={() => handleRemoveItemClick(index)}
                            >
                                移除
                            </Button>
                        </ListItem>
                    ))}
                </List>
            </DialogContent>
            <div className={classes.row}>
                <Button
                    variant="contained"
                    size="large"
                    disableElevation
                    fullWidth
                    style={{
                        backgroundColor: '#F2F2F2',
                        borderRadius: 0,
                        color: '#555555',
                    }}
                    onClick={handleCancelClick}
                >
                    取消
                </Button>
                <Button
                    variant="contained"
                    size="large"
                    disableElevation
                    color="primary"
                    fullWidth
                    style={{ borderRadius: 0 }}
                    onClick={handleCompleteClick}
                >
                    確定
                </Button>
            </div>
        </Dialog>
    );
}

export { SelectPhotosDialog };
