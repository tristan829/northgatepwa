import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme) => ({
    row: {
        display: 'flex',
        marginTop: 8,
    },
    photoListItem: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: theme.spacing(1, 0),
        padding: theme.spacing(1, 0),
        borderBottom: '1px solid #BBBBBB',
    },
    photoListItemImg: {
        width: 120,
        height: 120,
        objectFit: 'contain',
    },
}));
