import React, { useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog/Dialog';
import TextField from '@material-ui/core/TextField';
import makeStyles from '@material-ui/core/styles/makeStyles';

SerialNumberInputDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onComplete: PropTypes.func.isRequired,
};

SerialNumberInputDialog.defaultProps = {
    onComplete: (serialNumber) => {},
};

export const useStyles = makeStyles(() => ({
    dialogContent: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        overflowY: 'hidden',
    },
    signatureCanvasContainer: {
        flex: 1,
        height: '100%',
    },
    signatureCanvas: {
        width: '100%',
        height: '100%',
        border: '2px solid #DDDDDD',
    },
    row: {
        display: 'flex',
        marginTop: 8,
        '& > *': {
            flex: 1,
        },
    },
}));

function SerialNumberInputDialog(props) {
    const {
        open,
        onClose,
        onComplete,
    } = props;
    const classes = useStyles();
    const [serialNumber, setSerialNumber] = useState('');

    function handleCancelClick() {
        onClose();
    }

    function handleCompleteClick() {
        onComplete(serialNumber);
        setSerialNumber('');
    }

    return (
        <Dialog open={open}>
            <DialogTitle style={{ textAlign: 'center', borderBottom: '1px solid #DDDDDD', marginBottom: 8 }}>
                手動輸入序號
            </DialogTitle>
            <DialogContent className={classes.dialogContent}>
                <TextField
                    variant="outlined"
                    placeholder="序號"
                    value={serialNumber}
                    onChange={(e) => setSerialNumber(e.target.value)}
                />
            </DialogContent>
            <div className={classes.row}>
                <Button
                    variant="contained"
                    size="large"
                    disableElevation
                    fullWidth
                    style={{
                        backgroundColor: '#F2F2F2',
                        borderRadius: 0,
                        color: '#555555',
                    }}
                    onClick={handleCancelClick}
                >
                    取消
                </Button>
                <Button
                    variant="contained"
                    size="large"
                    disableElevation
                    color="primary"
                    fullWidth
                    style={{ borderRadius: 0 }}
                    onClick={handleCompleteClick}
                >
                    確定
                </Button>
            </div>
        </Dialog>
    );
}

export { SerialNumberInputDialog };
