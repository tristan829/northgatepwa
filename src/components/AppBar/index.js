import React from 'react';
import * as PropTypes from 'prop-types';

import Toolbar from '@material-ui/core/Toolbar';
import { default as MuiAppBar }  from '@material-ui/core/AppBar';

import { useStyles } from './style';

AppBar.propTypes = {
    title: PropTypes.string.isRequired,
};

function AppBar(props) {
    const {
        title,
    } = props;
    const classes = useStyles();

    return (
        <MuiAppBar position="sticky">
            <Toolbar className={classes.toolbar}>
                <div className={classes.center}>
                    {title}
                </div>
            </Toolbar>
        </MuiAppBar>
    );
}

export { AppBar };
