import createStyles from '@material-ui/core/styles/createStyles';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles(() => createStyles({
    toolbar: {
        position: 'relative',
        display: 'flex',
        justifyContent: 'space-between',
    },
    center: {
        position: 'absolute',
        left: '50%',
        transform: 'translateX(-50%)',
    },
}));
