import React from 'react';
import * as PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

SearchInput.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.string.isRequired,
};

function SearchInput(props) {
    const {
        value,
        onChange,
    } = props;

    return (
        <TextField
            placeholder="關鍵字搜尋"
            variant="outlined"
            value={value}
            onChange={onChange}
            fullWidth
        />
    );
}

export { SearchInput };
