import React from 'react';
import * as PropTypes from 'prop-types';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


import { useItemStyles } from './style';

MenuItem.propTypes = {
    icon: PropTypes.node.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func,
};

MenuItem.defaultProps = {
    onClick: () => {},
};

function MenuItem(props) {
    const {
        icon,
        text,
        onClick,
    } = props;
    const classes = useItemStyles();

    return (
        <Button className={classes.item} onClick={onClick}>
            <Typography variant="h3">
                {icon}
            </Typography>
            <span className={classes.text}>{text}</span>
            <Icon icon={faChevronRight} />
        </Button>
    );
}

export { MenuItem };
