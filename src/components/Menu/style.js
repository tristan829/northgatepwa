import makeStyles from '@material-ui/core/styles/makeStyles';

export const useMenuStyles = makeStyles(() => ({
    menu: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
        overflowY: 'auto',
    },
}));

export const useItemStyles = makeStyles((theme) => ({
    item: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 0,
        padding: theme.spacing(4, 4),
        fontSize: theme.typography.h5.fontSize,
        borderBottomWidth: 1,
        borderBottomColor: theme.palette.divider,
        borderBottomStyle: 'solid',
    },
    text: {
        margin: theme.spacing(0, 2),
    },
}));
