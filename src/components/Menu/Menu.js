import React from 'react';
import * as PropTypes from 'prop-types';
import { useMenuStyles } from './style';

Menu.propTypes = {
    children: PropTypes.node.isRequired,
};

function Menu(props) {
    const {
        children,
    } = props;
    const classes = useMenuStyles();

    return (
        <div className={classes.menu}>
            {children}
        </div>
    );
}

export { Menu };
