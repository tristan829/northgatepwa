import React, {
    useEffect,
} from 'react';
import * as PropTypes from 'prop-types';
import makeStyles from '@material-ui/core/styles/makeStyles';
import clsx from 'clsx';

Page.propTypes = {
    children: PropTypes.node.isRequired,
    title: PropTypes.string.isRequired,
    className: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
};

Page.defaultProps = {
    className: {},
};

const useStyle = makeStyles({
    root: {
        position: 'relative',
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
});

function Page(props) {
    

    const {
        children,
        title,
        className,
    } = props;
    const classes = useStyle();

    useEffect(() => {
        window.document.title = title;
    }, [title]);

    return (
        <div className={clsx(classes.root, className)}>
            { children }
        </div>
    );
}

export { Page };
