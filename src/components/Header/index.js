import React from 'react';
import * as PropTypes from 'prop-types';
import makeStyles from '@material-ui/core/styles/makeStyles';

Header.propTypes = {
    children: PropTypes.node.isRequired,
};

const useStyles = makeStyles((theme) => ({
    header: {
        padding: theme.spacing(2, 4),
        '& > *': {
            margin: theme.spacing(1, 0),
        },
    },
}));

function Header(props) {
    const { children } = props;
    const classes = useStyles();
    return (
        <div className={classes.header}>
            {children}
        </div>
    );
}

export { Header };
