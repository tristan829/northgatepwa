import React from 'react';
import * as PropTypes from 'prop-types';
import {
    List,
    ListHeader,
    ListItem,
    ListCell,
    ListContainer,
} from '../List';

CustomerTable.propTypes = {
    freightBillNumber: PropTypes.string.isRequired,
    data: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        inCar: PropTypes.bool.isRequired,
        customerName: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired,
    })).isRequired,
    onItemClick: PropTypes.func,
};

CustomerTable.defaultProps = {
    onItemClick: () => {},
};

function CustomerTable(props) {
    const { data, onItemClick, freightBillNumber } = props;

    return (
        <ListContainer>
            <ListHeader>
                <ListCell width={80}>狀態</ListCell>
                <ListCell width={120}>序號</ListCell>
                <ListCell width={120}>提單號碼</ListCell>
                <ListCell width={120}>客戶名稱</ListCell>
                <ListCell width={180}>電話</ListCell>
                <ListCell width={280}>地址</ListCell>
            </ListHeader>
            <List>
                {data.map((item) => (
                    <ListItem onClick={() => onItemClick(item.id)}>
                        <ListCell width={80}>
                            <span style={{ color: item.inCar ? '#28a745' : '#dc3545' }}>
                                {item.inCar ? '已上車' : '未上車'}
                            </span>
                        </ListCell>
                        <ListCell width={120}>{item.serialNumber}</ListCell>
                        <ListCell width={120}>{freightBillNumber}</ListCell>
                        <ListCell width={120}>{item.customerName}</ListCell>
                        <ListCell width={180}>{item.phone}</ListCell>
                        <ListCell width={280}>{item.address}</ListCell>
                    </ListItem>
                ))}
            </List>
        </ListContainer>
    );
}

export { CustomerTable };
