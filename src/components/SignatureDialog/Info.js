import React from 'react';
import * as PropTypes from 'prop-types';

import { useInfoStyles } from './style';

Info.propTypes = {
    itemAmount: PropTypes.number.isRequired,
    collectOnDeliveryAmount: PropTypes.number.isRequired,
    dutyAmount: PropTypes.number.isRequired,
    isCollected: PropTypes.bool.isRequired,
    isDutyCollected: PropTypes.bool.isRequired,
    receiverName: PropTypes.string.isRequired,
};

function Info(props) {
    const {
        itemAmount,
        collectOnDeliveryAmount,
        isCollected,
        receiverName,
        dutyAmount,
        isDutyCollected,
    } = props;
    const classes = useInfoStyles();

    return (
        <div className={classes.container}>
            <div className={classes.row}>
                <div className={classes.col}>客戶名稱: {receiverName}</div>
            </div>
            <div className={classes.row}>
                <div className={classes.col}>共 {itemAmount} 件</div>
            </div>
            <div className={classes.row}>
                <div className={classes.col}>代收金額: ${collectOnDeliveryAmount}</div>
            </div>
            <div className={classes.row}>
                <div className={classes.col}>代收稅金: ${dutyAmount}</div>
            </div>
        </div>
    );
}

export { Info };
