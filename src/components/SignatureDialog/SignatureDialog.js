import React, { useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog/Dialog';
import SignatureCanvas from 'react-signature-canvas';
import { useStyles } from './style';
import { Info } from './Info';

SignatureDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onComplete: PropTypes.func.isRequired,
    itemAmount: PropTypes.number.isRequired,
    collectOnDeliveryAmount: PropTypes.number.isRequired,
    dutyAmount: PropTypes.number.isRequired,
    isCollected: PropTypes.bool.isRequired,
    isDutyCollected: PropTypes.bool.isRequired,
    receiverName: PropTypes.string.isRequired,
};

function SignatureDialog(props) {
    const {
        open,
        onClose,
        onComplete,
        itemAmount,
        collectOnDeliveryAmount,
        isCollected,
        receiverName,
        dutyAmount,
        isDutyCollected,
    } = props;
    const classes = useStyles();
    const [canvasSize, setCanvasSize] = useState({
        width: 0,
        height: 0,
    });
    const signatureCanvasContainer = useRef(null);
    const signatureCanvas = useRef(null);

    function handleCancelClick() {
        onClose();
        signatureCanvas.current.clear();
    }

    function handleResetClick() {
        signatureCanvas.current.clear();
    }

    function handleCompleteClick() {
        function dataURLtoFile(dataurl, filename) {

            let arr = dataurl.split(','),
                mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]),
                n = bstr.length,
                u8arr = new Uint8Array(n);

            while (n--) {
                u8arr[n] = bstr.charCodeAt(n);
            }

            return new File([u8arr], filename, {type:mime});
        }

        const file = dataURLtoFile(signatureCanvas.current.toDataURL(), 'signature.png');
        onComplete(file);
        signatureCanvas.current.clear();
    }

    function handleDialogEntered() {
        if (signatureCanvasContainer?.current) {
            setCanvasSize({
                width: signatureCanvasContainer.current.clientWidth,
                height: signatureCanvasContainer.current.clientHeight,
            });
        }
    }

    return (
        <Dialog
            open={open}
            fullScreen
            onEntered={handleDialogEntered}
        >
            <DialogTitle style={{ textAlign: 'center', borderBottom: '1px solid #DDDDDD', marginBottom: 8 }}>
                簽名
            </DialogTitle>
            <DialogContent className={classes.dialogContent}>
                <Info
                    itemAmount={itemAmount}
                    collectOnDeliveryAmount={collectOnDeliveryAmount}
                    dutyAmount={dutyAmount}
                    isCollected={isCollected}
                    receiverName={receiverName}
                    isDutyCollected={isDutyCollected}
                />
                <div
                    ref={signatureCanvasContainer}
                    style={{ height: '100%' }}
                >
                    <SignatureCanvas
                        ref={signatureCanvas}
                        canvasProps={{
                            width: canvasSize.width,
                            height: canvasSize.height,
                            className: classes.signatureCanvas,
                        }}
                    />
                </div>
            </DialogContent>
            <div className={classes.row}>
                <Button
                    variant="contained"
                    size="large"
                    disableElevation
                    fullWidth
                    style={{
                        backgroundColor: '#F2F2F2',
                        borderRadius: 0,
                        color: '#555555',
                    }}
                    onClick={handleCancelClick}
                >
                    取消
                </Button>
                <Button
                    variant="contained"
                    size="large"
                    disableElevation
                    fullWidth
                    color="secondary"
                    style={{ borderRadius: 0 }}
                    onClick={handleResetClick}
                >
                    清除
                </Button>
                <Button
                    variant="contained"
                    size="large"
                    disableElevation
                    color="primary"
                    fullWidth
                    style={{ borderRadius: 0 }}
                    onClick={handleCompleteClick}
                >
                    確定
                </Button>
            </div>
        </Dialog>
    );
}

export { SignatureDialog };
