import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles(() => ({
    dialogContent: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        overflowY: 'hidden',
    },
    signatureCanvasContainer: {
        flex: 1,
        height: '100%',
    },
    signatureCanvas: {
        width: '100%',
        height: '100%',
        border: '2px solid #DDDDDD',
    },
    row: {
        display: 'flex',
        marginTop: 8,
        '& > *': {
            flex: 1,
        },
    },
}));

export const useInfoStyles = makeStyles((theme) => ({
    container: {
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        marginBottom: theme.spacing(1),
    },
    row: {
        width: '100%',
        display: 'flex',
    },
    col: {
        flex: 1,
    },
}));
