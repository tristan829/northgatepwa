import React from 'react';
import * as PropTypes from 'prop-types';
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { faUpload } from '@fortawesome/fontawesome-free';
import Button from '@material-ui/core/Button';

UploadButton.propTypes = {
    onClick: PropTypes.func.isRequired,
};

function UploadButton(props) {
    const { onClick } = props;

    return (
        <Button
            variant="contained"
            color="default"
            size="large"
            onClick={onClick}
            startIcon={<Icon icon={faUpload} />}
            style={{ backgroundColor: '#17A2B8', color: '#FFFFFF' }}
            disableElevation
            fullWidth
        >
            上傳伺服器
        </Button>
    );
}

export { UploadButton };
