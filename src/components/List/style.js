import makeStyles from '@material-ui/core/styles/makeStyles';

export const useListContainerStyles = makeStyles(() => ({
    container: {
        height: '100%',
        overflowX: 'auto',
        overflowY: 'hidden',
    },
}));

export const useListStyles = makeStyles((theme) => ({
    list: {
        width: 'fit-content',
        minWidth: '100%',
        maxWidth: '100%',
        height: '100%',
        margin: 0,
        padding: 0,
        listStyle: 'none',
        overflowX: 'auto',
    },
}));

export const useListItemStyles = makeStyles(() => ({
    item: {
        display: 'flex',
    },
}));

export const useListHeaderStyles = makeStyles((theme) => ({
    header: {
        display: 'flex',
        padding: 0,
        fontSize: theme.typography.h6.fontSize,
        fontWeight: 'bold',
        borderTop: '1px solid rgba(224, 224, 224, 1)',
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
        width: 'fit-content',
        minWidth: '100%',
        whiteSpace: 'nowrap',
    },
}));

export const useListCellStyles = makeStyles((theme) => ({
    cell: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(2),
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
        fontSize: theme.typography.body1.fontSize,
    },
}));
