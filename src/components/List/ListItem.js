import React from 'react';
import * as PropTypes from 'prop-types';
import { useListItemStyles } from './style';

ListItem.propTypes = {
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func,
    className: PropTypes.string,
};

ListItem.defaultProps = {
    onClick: () => {},
    className: '',
};

function ListItem(props) {
    const { children, onClick, className } = props;
    const classes = useListItemStyles();

    return (
        <li className={`${classes.item} ${className}`} onClick={onClick}>
            {children}
        </li>
    );
}

export { ListItem };
