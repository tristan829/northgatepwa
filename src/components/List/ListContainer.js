import React from 'react';
import * as PropTypes from 'prop-types';

import { useListContainerStyles } from './style';

ListContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

function ListContainer(props) {
    const { children } = props;
    const classes = useListContainerStyles();
    return (
        <div className={classes.container}>
            {children}
        </div>
    );
}

export { ListContainer };
