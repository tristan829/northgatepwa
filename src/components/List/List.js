import React from 'react';
import * as PropTypes from 'prop-types';

import { useListStyles } from './style';
import {ListCell} from './ListCell';
import {ListContainer} from './ListContainer';
import {ListHeader} from './ListHeader';
import {ListItem} from './ListItem';



List.propTypes = {
    children: PropTypes.element.isRequired,
};

function List(props) {
    const { children } = props;
    const classes = useListStyles();
    return (
        <ul className={classes.list}>
            {children}
        </ul>
    );
}

export { List, ListCell, ListContainer, ListHeader, ListItem };
