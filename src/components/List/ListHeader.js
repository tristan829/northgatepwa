import React from 'react';
import * as PropTypes from 'prop-types';
import { useListHeaderStyles } from './style';

ListHeader.propTypes = {
    children: PropTypes.node.isRequired,
};

function ListHeader(props) {
    const { children } = props;
    const classes = useListHeaderStyles();

    return (
        <div className={classes.header}>
            {children}
        </div>
    );
}

export { ListHeader };
