import React from 'react';
import * as PropTypes from 'prop-types';
import { useListCellStyles } from './style';

ListCell.propTypes = {
    children: PropTypes.node.isRequired,
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

ListCell.defaultProps = {
    width: undefined,
};

function ListCell(props) {
    const { children, width } = props;
    const classes = useListCellStyles();
    const style = width === undefined ? {} : { width, minWidth: width };

    return (
        <div className={classes.cell} style={style}>
            {children}
        </div>
    );
}

export { ListCell };
